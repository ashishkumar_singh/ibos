package com.intellect.ibos.Utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.prowidesoftware.swift.model.SwiftBlock1;
import com.prowidesoftware.swift.model.SwiftBlock1Field;
import com.prowidesoftware.swift.model.SwiftBlock2;
import com.prowidesoftware.swift.model.SwiftBlock2Output;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.field.Field13D;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.field.Field25;
import com.prowidesoftware.swift.model.field.Field28;
import com.prowidesoftware.swift.model.field.Field28C;
import com.prowidesoftware.swift.model.field.Field34F;
import com.prowidesoftware.swift.model.field.Field60F;
import com.prowidesoftware.swift.model.field.Field61;
import com.prowidesoftware.swift.model.field.Field62F;
import com.prowidesoftware.swift.model.field.Field65;
import com.prowidesoftware.swift.model.field.Field86;
import com.prowidesoftware.swift.model.field.Field90C;
import com.prowidesoftware.swift.model.field.Field90D;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt9xx.MT941;
import com.prowidesoftware.swift.model.mt.mt9xx.MT942;


public class MTXXXFormatter {
	
	private static long referenceNo = 1000000000;
	private static long statementSeqNo = 1;
	private static long statementSeqNoForMT941 = 1;
	private static long sessionNumber = 2000;
	private static long sequenceNumber = 234567;
	
	public String formatMT942Message(String obj, String senderBIC, String receiverBic) {
		
		System.out.println(" Entering formatter : "+  obj);
		MT942 mt942 = new MT942();
		JSONObject pMT942JsonObj = new JSONObject(obj);
		JSONObject accountsFromjson = pMT942JsonObj.getJSONObject(IBOSConstants.account);
		JSONObject transactionsFromIntessa =  pMT942JsonObj.getJSONObject("transactions");
		
		JSONArray bookedIntessaTransactions  = transactionsFromIntessa.getJSONArray("booked");
		JSONArray PendingIntessaTransactions  = transactionsFromIntessa.getJSONArray("pending");
		
		BigDecimal debit90Damount = BigDecimal.ZERO;
		BigDecimal credit90Camount = BigDecimal.ZERO;
		
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sf2 = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat sf3 = new SimpleDateFormat("MMdd");
		
		
		
		long debitTxns = 0;
		long creditTxns = 0;
		
		try {
		//mt942.setSender(senderBIC);
		mt942.setReceiver(receiverBic);
		
		SwiftMessage m = mt942.getSwiftMessage();
		SwiftBlock1 swbl= new SwiftBlock1();
		if(sessionNumber>=9999) sessionNumber=1234;
		//System.out.println(m.getBlock1() + " @@@@@@@@@@@");
		swbl.setSessionNumber(String.valueOf(sessionNumber++));
		if(sequenceNumber>=999999) sequenceNumber=123456;
		swbl.setSequenceNumber(String.valueOf(sequenceNumber++));
		swbl.setSender(senderBIC);
		swbl.setApplicationId("F");
		swbl.setServiceId("01");
		
		m.setBlock1(swbl);
		Date senderInputTime = new Date();
		SimpleDateFormat sfhhmm = new SimpleDateFormat("HHMM");
	
		String MIRDate = sf2.format(senderInputTime);
		SwiftBlock2 sw2 = new SwiftBlock2Output("942", sfhhmm.format(senderInputTime), MIRDate, receiverBic, String.valueOf(sessionNumber++), String.valueOf(sequenceNumber++), MIRDate, sfhhmm.format(senderInputTime), "N");
		
		m.setBlock2(sw2);
		mt942.setSwiftMessage(m);
		
		
		if(!accountsFromjson.has("currency")) {
			 accountsFromjson.put(IBOSConstants.currency,"EUR");
			 System.out.println("accounts json : "+accountsFromjson);
		}
		
		Field20 f20 = new Field20();
		f20.setReference(String.valueOf(referenceNo++));
		mt942.addField(f20);
	
		//Tag 25
		Field25 f25 = new Field25();
		String iban = (String) accountsFromjson.get(IBOSConstants.iban);
		if(iban.contains("-")) {
			iban=iban.substring(0,iban.lastIndexOf("-"));
		}
		if(iban.contains("_")) {
			iban=iban.substring(0,iban.lastIndexOf("_"));
		}
		
		f25.setAccount(iban);
		
		//f25.setAccount((String) accountsFromjson.get(IBOSConstants.iban));
		mt942.addField(f25);
		
		statementSeqNo++;
		Field28C f28c = new Field28C();
		f28c.setStatementNumber(statementSeqNo+1000);
		f28c.setSequenceNumber(statementSeqNo);
		mt942.addField(f28c);
		
		List<Float> list = new ArrayList();
		for(Object bookedTxns : bookedIntessaTransactions) {
			JSONObject bookedTxnJson = (JSONObject) bookedTxns;
			JSONObject transactionAmount = bookedTxnJson.getJSONObject("transactionAmount");
			
			
			try {
				Float f = Float.parseFloat((String)transactionAmount.get("amount"));
				String strDouble = String.format("%.2f", f);
				
			list.add(Math.abs(f));
			System.out.println(list);
			}
			catch(Exception e ) {
				e.printStackTrace();
				list.add(Float.parseFloat("0.0"));
			}
		
		
		}
		
		if(!list.isEmpty()) {
			List<Integer> sortedlist = new ArrayList(list); 
			Collections.sort(sortedlist);
			Field34F f34 = new Field34F();
			try{
			//f34.setAmount(String.format("%.2f", sortedlist.get(0)));
			f34.setAmount("0,");
			//throw new Exception();
			}
			catch(Exception e) {
				e.printStackTrace();
				f34.setAmount("0,");
			}
			f34.setCurrency((String) accountsFromjson.get(IBOSConstants.currency));
			f34.setDCMark("D");
			mt942.addField(f34);
		}
		
//		for(Object pendingTxns : PendingIntessaTransactions) {
//			JSONObject pendingTxnsJson = (JSONObject) pendingTxns;
//			JSONObject transactionAmount = pendingTxnsJson.getJSONObject("transactionAmount");
//			
		Field34F f34 = new Field34F();
		f34.setAmount("0,");
		f34.setCurrency((String) accountsFromjson.get(IBOSConstants.currency));
		f34.setDCMark("C");
		mt942.addField(f34);
//		}
		
		Field13D f13D = new Field13D();
		Date date1 = new Date();
		//Calendar c = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyMMdd");
		formatter.setTimeZone(TimeZone.getTimeZone("CET"));
		f13D.setDate(formatter.format(date1));
		formatter = new SimpleDateFormat("HHmm");
		formatter.setTimeZone(TimeZone.getTimeZone("CET"));
		f13D.setTime(formatter.format(date1));
		f13D.setSign("+");
		f13D.setOffset("0100");
		mt942.addField(f13D);
		
		for (Object bookedTxns : bookedIntessaTransactions) {
			JSONObject bookedTxnJson = (JSONObject) bookedTxns;
			JSONObject transactionAmount = bookedTxnJson.getJSONObject("transactionAmount");
			
			Field61 field61 = new Field61();
			try {
			String date = sf3.format(sf.parse(bookedTxnJson.getString("bookingDate")));
			String date2 =  sf2.format(sf.parse(bookedTxnJson.getString("valueDate")));
			
			field61.setEntryDate(date);
			field61.setValueDate(date2);
			}
			catch(Exception e) {
				field61.setEntryDate(sf2.format(new Date()));
				field61.setValueDate(sf2.format(new Date()));
			}
			
			String amt=(String) transactionAmount.get("amount");
			
			if(amt.contains("-"))
			{
				field61.setDCMark("D");
				amt=amt.substring(1);
			}else
			{
				field61.setDCMark("C");
			}
			
			String fundsCode="R";
			try {
				fundsCode = accountsFromjson.getString(IBOSConstants.currency).substring(2);;
			}
			catch (Exception e) {
				fundsCode = "R";
			}
			field61.setFundsCode(fundsCode);
			field61.setAmount(amt.replace('.', ','));
			field61.setIdentificationCode("NTRF");
			
			
			if(bookedTxnJson.has("transactionId"))
			{
			String TxnId = bookedTxnJson.getString("transactionId");
			try {
				if(TxnId.length()>16) {
					TxnId = TxnId.substring(2, TxnId.length());
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
			field61.setReferenceForTheAccountOwner(TxnId);
			
			//field61.setReferenceOfTheAccountServicingInstitution((String) accountsFromjson.get(IBOSConstants.iban));
		//	field61.setSupplementaryDetails("/REMI/");
			
			}else
			{
				String AccountOwner="NONREF";
				String SuppDetails="TRANSACTION ID NOT PROVIDED BY AHB";
				
				field61.setReferenceForTheAccountOwner(AccountOwner);
				field61.setSupplementaryDetails(SuppDetails);
				
			}
			mt942.addField(field61);
			Field86 f86  = new Field86();
			String name = "";
			
			if(bookedTxnJson.has("debtorName")) {
				name =bookedTxnJson.getString("debtorName") ;
				try {
					debit90Damount=debit90Damount.add(new BigDecimal((String)transactionAmount.get("amount")).abs());
				}catch (Exception e) {
					debit90Damount=debit90Damount.add(BigDecimal.ONE);
				}
				debitTxns++;
			}
			else {
				try {
					credit90Camount=credit90Camount.add(new BigDecimal((String)transactionAmount.get("amount")).abs());
					}catch (Exception e) {
						credit90Camount=credit90Camount.add(BigDecimal.ONE);
					}
				creditTxns++;
			}
			if(bookedTxnJson.has("creditorName")) {
				name = bookedTxnJson.getString("creditorName") ;
				
				
			}
			f86.setNarrative("/NAME/"+name);
			//mt942.addField(f86);
			
			
		}
		
		for (Object pendingTxnso : PendingIntessaTransactions) {
			/*
			 * JSONObject pendingTxns = (JSONObject) pendingTxnso; JSONObject
			 * transactionAmount = pendingTxns.getJSONObject("transactionAmount");
			 * 
			 * Field61 field61 = new Field61(); try { String date =
			 * sf2.format(sf.parse(pendingTxns.getString("bookingDate"))); String date2 =
			 * sf2.format(sf.parse(pendingTxns.getString("valueDate")));
			 * 
			 * field61.setEntryDate(date); field61.setValueDate(date2); } catch(Exception e)
			 * { field61.setEntryDate(sf2.format(new Date()));
			 * field61.setValueDate(sf2.format(new Date())); } field61.setDCMark("D");
			 * String fundsCode="R"; try { fundsCode =
			 * accountsFromjson.getString(IBOSConstants.currency).substring(2);; } catch
			 * (Exception e) { fundsCode = "R"; } field61.setFundsCode(fundsCode);
			 * field61.setAmount((String)transactionAmount.get("amount"));
			 * field61.setIdentificationCode("NMSC");
			 * field61.setReferenceForTheAccountOwner(pendingTxns.getString("transactionId")
			 * ); field61.setReferenceOfTheAccountServicingInstitution((String)
			 * accountsFromjson.get(IBOSConstants.iban));
			 * field61.setSupplementaryDetails(""); mt942.addField(field61);
			 * 
			 * Field86 f86 = new Field86(); String name = "";
			 * 
			 * if(pendingTxns.has("debtorName")) {name =pendingTxns.getString("debtorName")
			 * ;} if(pendingTxns.has("creditorName")) {name =
			 * pendingTxns.getString("creditorName") ; } f86.setNarrative("/NAME/"+name);
			 * mt942.addField(f86);
			 * 
			 * 
			 */}

		Field90D f90D = new Field90D();
		f90D.setAmount(debit90Damount);
		try {
		f90D.setCurrency((String) accountsFromjson.get(IBOSConstants.currency));
		}
		catch(Exception e) {
			f90D.setCurrency("EUR");	
		}
		f90D.setNumber(debitTxns);
		//mt942.addField(f90D);
		
		Field90C f90c = new Field90C();
		f90c.setAmount(credit90Camount);
		try {
			f90c.setCurrency((String) accountsFromjson.get(IBOSConstants.currency));
		}
		catch(Exception e) {
			f90c.setCurrency("EUR");	
		}
		f90c.setNumber(creditTxns);
		//mt942.addField(f90c);
		
		
		//90D
		//90C
		System.out.println(mt942.message());
		}
		catch(Exception e) {
			e.printStackTrace();
			System.err.println("Not able to format the MT942 message : "+e);
		}
		return mt942.message();
		
		
	}

	public String formatMT941Message(String obj, String senderBIC, String receiverBic) {
		
		statementSeqNoForMT941++;
		System.out.println(" Entering formatter : "+  obj);
		MT941 mt941 = new MT941();
		JSONObject pMT941JsonObj = new JSONObject(obj);
		if(senderBIC=="NDEAFIHHAXXX")
		{
			System.out.println("Inside Nordea MT941");
		//JSONObject accountsFromjson = pMT941JsonObj.getJSONObject(IBOSConstants.account);  change for standardized structure
		JSONArray balancesforIntessaAndPBZ = pMT941JsonObj.getJSONArray(IBOSConstants.balances);
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sf2 = new SimpleDateFormat("yyMMdd");
		
		try {
	
		//mt941.setSender(senderBIC);
		mt941.setReceiver(receiverBic);
		
		SwiftMessage m = mt941.getSwiftMessage();
		if(sessionNumber>=9999) sessionNumber=1234;
		if(sequenceNumber>=999999) sequenceNumber=123456;
		SwiftBlock1 swbl= new SwiftBlock1("F","01",senderBIC,String.valueOf(sessionNumber),String.valueOf(sequenceNumber));
//		if(sessionNumber>=9999) sessionNumber=1234;
//		//System.out.println(m.getBlock1() + " @@@@@@@@@@@");
//		swbl.setSessionNumber(String.valueOf(sessionNumber++));
//		System.out.println("swbl : "+swbl.getSessionNumber());
//		if(sequenceNumber>=999999) sequenceNumber=123456;
//		swbl.setSequenceNumber(String.valueOf(sequenceNumber++));
//		//swbl.setSequenceNumber(String.valueOf(sequenceNumber++));
//		swbl.setSender(senderBIC);
//		swbl.setApplicationId("F");
//		swbl.setServiceId("01");
		System.out.println("@@@@@@@@@@@@@@@@@ "+swbl.getLogicalTerminal());
		
		Date senderInputTime = new Date();
		SimpleDateFormat sfhhmm = new SimpleDateFormat("HHMM");
	
		String MIRDate = sf2.format(senderInputTime);
		SwiftBlock2 sw2 = new SwiftBlock2Output("941", sfhhmm.format(senderInputTime), MIRDate, receiverBic, String.valueOf(sessionNumber++), String.valueOf(sequenceNumber++), MIRDate, sfhhmm.format(senderInputTime), "N");
		m.setBlock2(sw2);
		
		
		m.setBlock1(swbl);
		mt941.setSwiftMessage(m);
		
		if(!pMT941JsonObj.has("currency")) {
			pMT941JsonObj.put(IBOSConstants.currency,"EUR");
			 System.out.println("accounts json : "+pMT941JsonObj);
		}
		
		Field20 f20 = new Field20();
		f20.setReference(String.valueOf(referenceNo++));
		mt941.addField(f20);
		
		    //Tag 25
		Field25 f25 = new Field25();
		String iban = (String) pMT941JsonObj.get(IBOSConstants.iban);
		if(iban.contains("-")) {
			iban=iban.substring(0,iban.lastIndexOf("-"));
		 }
		f25.setAccount(iban);
		mt941.addField(f25);
		
		Field28 f28 = new Field28();
		f28.setStatementNumber(statementSeqNoForMT941+1000);
		f28.setSequenceNumber(statementSeqNoForMT941);
		mt941.addField(f28);
		
		
		Field13D f13D = new Field13D();
		Date date1 = new Date();
		//Calendar c = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyMMdd");
		formatter.setTimeZone(TimeZone.getTimeZone("CET"));
		f13D.setDate(formatter.format(date1));
		formatter = new SimpleDateFormat("HHmm");
		formatter.setTimeZone(TimeZone.getTimeZone("CET"));
		f13D.setTime(formatter.format(date1));
		f13D.setSign("+");
		f13D.setOffset("0100");
		mt941.addField(f13D);
		
		
		
				
		
		for(Object balance : balancesforIntessaAndPBZ) {
			JSONObject balanceJson = (JSONObject) balance;
			System.out.println(" balanceJson.getString(IBOSConstants.balanceType):"+ balanceJson.getString(IBOSConstants.balanceType));
			if("forwardAvailable".equalsIgnoreCase(balanceJson.getString(IBOSConstants.balanceType))){
				Field65 f65 = new Field65();
				JSONObject transactionDetails = balanceJson.getJSONObject(IBOSConstants.balanceAmount);
				
				f65.setAmount(transactionDetails.getString("amount"));
				f65.setCurrency(transactionDetails.getString("currency"));
				try {
					String date = sf2.format(sf.parse(balanceJson.getString("lastChangedDateTime")));
				f65.setDate(date);
				}
				catch(Exception e) {
					f65.setDate(sf2.format(new Date()));
				}
				f65.setDCMark("D");
				//mt941.addField(f65);
			}
			if("openingBooked".equalsIgnoreCase(balanceJson.getString(IBOSConstants.balanceType))){
				Field60F f60f = new Field60F();
				JSONObject transactionDetails = balanceJson.getJSONObject(IBOSConstants.balanceAmount);
				String amount = transactionDetails.getString("amount").replace('.', ',');
				
				if(amount.contains("-")) {
					f60f.setDCMark("D");
					amount= amount.substring(1);
					}
					else {
						f60f.setDCMark("C");
					}
				
				f60f.setAmount(amount);
				f60f.setCurrency(transactionDetails.getString("currency"));
				try {
					String date = sf2.format(sf.parse(balanceJson.getString("lastChangedDateTime")));
				f60f.setDate(date);
				}
				catch(Exception e) {
					f60f.setDate(sf2.format(new Date()));
				}
				
				mt941.addField(f60f);
				
			}
			if("closingBooked".equalsIgnoreCase(balanceJson.getString(IBOSConstants.balanceType))){
				Field62F f62f = new Field62F();
				JSONObject transactionDetails = balanceJson.getJSONObject(IBOSConstants.balanceAmount);
				String amount = transactionDetails.getString("amount").replace('.', ',');
               if(amount.contains("-")) {
					
					f62f.setDCMark("D");
					 amount= amount.substring(1);
				}
				else {
					f62f.setDCMark("C");
				}
				
				f62f.setAmount(amount);
				f62f.setCurrency(transactionDetails.getString("currency"));
				try {
					String date = sf2.format(sf.parse(balanceJson.getString("lastChangedDateTime")));
				f62f.setDate(date);
				}
				catch(Exception e) {
					f62f.setDate(sf2.format(new Date()));
				}
				
				
				mt941.addField(f62f);
			}
		}
		
		Field86 f86  = new Field86();
		String name = " NA";
		if(pMT941JsonObj.has("name")) {
			name = pMT941JsonObj.getString("name");
		}
		f86.setNarrative("/NAME/"+name);
		//mt941.addField(f86);
		
		System.out.println(mt941.message());
		}
		catch(Exception e) {
			e.printStackTrace();
			System.err.println("Not able to format the Nordes MT941 message : "+e);
		}
	}
	 if(senderBIC=="BCITITMMAXXX" || senderBIC=="PBZGHR2XAXXX" )
	 {
           System.out.println("inside ISP/PBZ MT941");

			JSONArray balancesforIntessaAndPBZ = pMT941JsonObj.getJSONArray(IBOSConstants.balances);
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sf2 = new SimpleDateFormat("yyMMdd");
			
			try {
		
			//mt941.setSender(senderBIC);
			mt941.setReceiver(receiverBic);
			
			SwiftMessage m = mt941.getSwiftMessage();
			if(sessionNumber>=9999) sessionNumber=1234;
			if(sequenceNumber>=999999) sequenceNumber=123456;
			SwiftBlock1 swbl= new SwiftBlock1("F","01",senderBIC,String.valueOf(sessionNumber),String.valueOf(sequenceNumber));
//			if(sessionNumber>=9999) sessionNumber=1234;
//			//System.out.println(m.getBlock1() + " @@@@@@@@@@@");
//			swbl.setSessionNumber(String.valueOf(sessionNumber++));
//			System.out.println("swbl : "+swbl.getSessionNumber());
//			if(sequenceNumber>=999999) sequenceNumber=123456;
//			swbl.setSequenceNumber(String.valueOf(sequenceNumber++));
//			//swbl.setSequenceNumber(String.valueOf(sequenceNumber++));
//			swbl.setSender(senderBIC);
//			swbl.setApplicationId("F");
//			swbl.setServiceId("01");
			System.out.println("@@@@@@@@@@@@@@@@@ "+swbl.getLogicalTerminal());
			
			Date senderInputTime = new Date();
			SimpleDateFormat sfhhmm = new SimpleDateFormat("HHMM");
		
			String MIRDate = sf2.format(senderInputTime);
			SwiftBlock2 sw2 = new SwiftBlock2Output("941", sfhhmm.format(senderInputTime), MIRDate, receiverBic, String.valueOf(sessionNumber++), String.valueOf(sequenceNumber++), MIRDate, sfhhmm.format(senderInputTime), "N");
			m.setBlock2(sw2);
			
			
			m.setBlock1(swbl);
			mt941.setSwiftMessage(m);
			
			if(!pMT941JsonObj.has("currency")) {
				pMT941JsonObj.put(IBOSConstants.currency,"EUR");
				 System.out.println("accounts json : "+pMT941JsonObj);
			}
			
			Field20 f20 = new Field20();
			f20.setReference(String.valueOf(referenceNo++));
			mt941.addField(f20);
			
			    //Tag 25
			Field25 f25 = new Field25();
			String iban = (String) pMT941JsonObj.get(IBOSConstants.iban);
			if(iban.contains("-")) {
				iban=iban.substring(0,iban.lastIndexOf("-"));
			 }
			f25.setAccount(iban);
			mt941.addField(f25);
			
		
			Field28 f28 = new Field28();
			f28.setStatementNumber(statementSeqNoForMT941+1000);
			f28.setSequenceNumber(statementSeqNoForMT941);
			mt941.addField(f28);
			
			Field13D f13D = new Field13D();
			Date date1 = new Date();
			//Calendar c = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("yyMMdd");
			formatter.setTimeZone(TimeZone.getTimeZone("CET"));
			f13D.setDate(formatter.format(date1));
			formatter = new SimpleDateFormat("HHmm");
			formatter.setTimeZone(TimeZone.getTimeZone("CET"));
			f13D.setTime(formatter.format(date1));
			f13D.setSign("+");
			f13D.setOffset("0100");
			mt941.addField(f13D);
			
			
			
					
			
			for(Object balance : balancesforIntessaAndPBZ) {
				JSONObject balanceJson = (JSONObject) balance;
				System.out.println(" balanceJson.getString(IBOSConstants.balanceType):"+ balanceJson.getString(IBOSConstants.balanceType));
				if("forwardAvailable".equalsIgnoreCase(balanceJson.getString(IBOSConstants.balanceType))){
					Field65 f65 = new Field65();
					JSONObject transactionDetails = balanceJson.getJSONObject(IBOSConstants.balanceAmount);
					
					f65.setAmount(transactionDetails.getString("amount"));
					f65.setCurrency(transactionDetails.getString("currency"));
					try {
						String date = sf2.format(sf.parse(balanceJson.getString("lastChangedDateTime")));
					f65.setDate(date);
					}
					catch(Exception e) {
						f65.setDate(sf2.format(new Date()));
					}
					f65.setDCMark("D");
					//mt941.addField(f65);
				}
				if("openingBooked".equalsIgnoreCase(balanceJson.getString(IBOSConstants.balanceType))){
					Field60F f60f = new Field60F();
					JSONObject transactionDetails = balanceJson.getJSONObject(IBOSConstants.balanceAmount);
					String amount = transactionDetails.getString("amount").replace('.', ',');
					if(amount.contains("-")) {
						f60f.setDCMark("D");
						amount= amount.substring(1);
						}
						else {
							f60f.setDCMark("C");
						}
					
					f60f.setAmount(amount);
					f60f.setCurrency(transactionDetails.getString("currency"));
					try {
						String date = sf2.format(sf.parse(balanceJson.getString("referenceDate"))); 
					f60f.setDate(date);
					}
					catch(Exception e) {
						f60f.setDate(sf2.format(new Date()));
					}
					
					mt941.addField(f60f);
					
				}
				if("closingBooked".equalsIgnoreCase(balanceJson.getString(IBOSConstants.balanceType))){
					Field62F f62f = new Field62F();
					JSONObject transactionDetails = balanceJson.getJSONObject(IBOSConstants.balanceAmount);
					String amount = transactionDetails.getString("amount").replace('.', ',');
                      if(amount.contains("-")) {
						f62f.setDCMark("D");
						  amount= amount.substring(1);
					}
					else {
						f62f.setDCMark("C");
					}
					f62f.setAmount(amount);
					f62f.setCurrency(transactionDetails.getString("currency"));
					try {
						String date = sf2.format(sf.parse(balanceJson.getString("lastChangeDateTime")));  
					f62f.setDate(date);
					}
					catch(Exception e) {
						f62f.setDate(sf2.format(new Date()));
					}
										
					mt941.addField(f62f);
				}
			}
			
			Field86 f86  = new Field86();
			String name = " NA";
			if(pMT941JsonObj.has("name")) {
				name = pMT941JsonObj.getString("name");
			}
			f86.setNarrative("/NAME/"+name);
			//mt941.addField(f86);
			
			System.out.println(mt941.message());
			}
			catch(Exception e) {
				e.printStackTrace();
				System.err.println("Not able to format the ISP/PBZ MT941 message : "+e);
			}
		
	 }
		return mt941.message();
		
		
	}
}
