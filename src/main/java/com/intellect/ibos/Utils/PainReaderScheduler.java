package com.intellect.ibos.Utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.intellect.ibos.DBConnectionUtil;
import com.intellect.ibos.controller.IbosMainController;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

/**
 * Scheduler for Payment Reader
 * 
 * @author nishanthi.mohan
 *
 */
@Component
public class PainReaderScheduler {

	@Value("${PAIN001_PATH}")
	private String incomingPain001Path;

	@Value("${PAIN001_BACKUP_PATH}")
	private String pain001BackUpPath;

	@Value("${PAIN001_FAILED_PATH}")
	private String pain001FailedPath;

	@Value("${PAIN002_BACKUP_PATH}")
	private String pain002BackUpPath;

	@Value("${PAIN002_FAILED_PATH}")
	private String pain002FailedPath;

	@Value("${POST_PBZ_PAYMENT_ENDPOINT}")
	String pbzPaymentEndpoint;

	@Value("${IBOS_TO_SAN}")
	private String ibosToSantander;

	@Value("${Pain002_PATH}")
	private String incomingPain002Path;

	@Value("${DB_url}")
	String url;

	@Value("${DB_username}")
	String userName;

	@Value("${DB_password}")
	String password;
	
	
	@Value("${TOMCAT_PATH_FAILED_FILES}")
	String localPath;

	@Autowired
	IbosMainController controller;

	/**
	 * Auto wiring PisDao class
	 */

	PisDao pisDao = new PisDao();

	/**
	 * Instantiating logger for logging purpose
	 */
	private static final Logger logger = LoggerFactory.getLogger(PainReaderScheduler.class);

	@Scheduled(fixedDelay = 60000)
	public void startProcessing() throws IOException, SftpException {

		SFTPConnectionUtilForPullingFiles lConnectionUtil = new SFTPConnectionUtilForPullingFiles();
		ChannelSftp lsftp = null;
		DBConnectionUtil dbUtil = new DBConnectionUtil();
		Connection conn = null;
		try {
			lsftp = lConnectionUtil.getSFTPConnectionforPullingFiles();
			/*
			 * if (lsftp == null || lsftp.isClosed() || !lsftp.isConnected()) {
			 * System.out.println("SFTP Connection is not established"); return; } else { if
			 * (logger.isInfoEnabled()) { logger.info("SFTP Connection Established"); } }
			 */
			readPainFromSFTP(incomingPain001Path, lsftp);
			readPain002FromSFTP(incomingPain002Path,lsftp);
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Exception occured while reading pain messages"+e.getMessage());
		} finally {
			if (lsftp != null) {
				lConnectionUtil.closeConnection();
			}
			dbUtil.mCloseResources(null, conn);

		}
	}

	private void readPain002FromSFTP(String incomingPain002Path, ChannelSftp lsftp) throws SftpException, IOException {
		lsftp.cd(incomingPain002Path);
		try {
			if (logger.isInfoEnabled()) {
				logger.info("Incoming Parser Path for Pain002-------> " + incomingPain002Path);
			}
			Vector<ChannelSftp.LsEntry> list = lsftp.ls("*PAIN02.*");

			for (ChannelSftp.LsEntry entry : list) {
				System.out.println(entry.getFilename());
				File tempFile = new File(entry.getFilename());
				lsftp.get(entry.getFilename(), tempFile.getAbsolutePath()); // copying to local file
				try {
					BufferedReader reader = new BufferedReader(new FileReader(tempFile));
					StringBuilder stringBuilder = new StringBuilder();
					char[] buffer = new char[10];
					while (reader.read(buffer) != -1) {
						stringBuilder.append(new String(buffer));
						buffer = new char[10];
					}
					reader.close();

					String pain002xmlFromSan = stringBuilder.toString();
					JSONObject xmlJSONObj = XML.toJSONObject(pain002xmlFromSan);

					if (logger.isInfoEnabled()) {
						logger.info("Incoming Pain002 xml From Santander-------> " + pain002xmlFromSan);
					}
					JSONObject document = xmlJSONObj.getJSONObject("Document");
					JSONObject cstmrPmtStsRpt = document.getJSONObject("CstmrPmtStsRpt");
					JSONObject orgnlPmtInfAndSts = cstmrPmtStsRpt.getJSONObject("OrgnlPmtInfAndSts");

					JSONObject txInfAndSts = orgnlPmtInfAndSts.getJSONObject("TxInfAndSts");
					String txnStatus = txInfAndSts.get("TxSts").toString();

					JSONObject orgnlGrpInfAndSts = cstmrPmtStsRpt.getJSONObject("OrgnlGrpInfAndSts");
					String OrgnlMsgId = orgnlGrpInfAndSts.get("OrgnlMsgId").toString();
					
					String OrgnlPmtInfId = orgnlPmtInfAndSts.get("OrgnlPmtInfId").toString();

					pisDao.insertPain002xmlFromSan(OrgnlMsgId,OrgnlPmtInfId, txnStatus, url, userName, password);
					lsftp.rename(incomingPain002Path + "/" + entry.getFilename(), pain002BackUpPath + "/"
							+ entry.getFilename() + new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
					tempFile.delete();
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("Error while parsing file " + entry.getFilename());
					lsftp.rename(incomingPain002Path + "/" + entry.getFilename(), pain002FailedPath + "/"
							+ entry.getFilename() + new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
					tempFile.delete();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception Occured while processing PAIN002 message..." + e);

		}
	}

	private void readPainFromSFTP(String incomingPain001Path, ChannelSftp lsftp) throws SftpException, IOException {
		try {
			lsftp.cd(incomingPain001Path);

			if (logger.isInfoEnabled()) {
				logger.info("Incoming Pain001 Parser Path-------> " + incomingPain001Path);
			}
			Vector<ChannelSftp.LsEntry> list = lsftp.ls("*PAIN01.*");

			for (ChannelSftp.LsEntry entry : list) {
				System.out.println(entry.getFilename());
				File tempFile = new File(entry.getFilename());
				lsftp.get(entry.getFilename(), tempFile.getAbsolutePath()); // copying to local file
				try {
					BufferedReader reader = new BufferedReader(new FileReader(tempFile));
					StringBuilder stringBuilder = new StringBuilder();
					char[] buffer = new char[10];
					while (reader.read(buffer) != -1) {
						stringBuilder.append(new String(buffer));
						buffer = new char[10];
					}
					reader.close();

					String xmlFileContent = stringBuilder.toString();
					convertSantoAHBReq(xmlFileContent,entry.getFilename());
					lsftp.rename(incomingPain001Path + "/" + entry.getFilename(), pain001BackUpPath + "/"
							+ entry.getFilename() + new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
					tempFile.delete();
				} catch (Exception e) {
					System.err.println("Error while parsing file " + entry.getFilename());
					lsftp.rename(incomingPain001Path + "/" + entry.getFilename(), pain001FailedPath + "/"
							+ entry.getFilename() + new SimpleDateFormat("DDMMYYHHMMSS").format(new Date()));
					tempFile.delete();
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Exception Occured while processing PAIN message..." + e.getLocalizedMessage());

		}

	}

	private void convertSantoAHBReq(String xmlFileContent,String incomingPain001DateSeq) throws IOException, SftpException {
		System.out.println("convertSantoAHBReq.incomingPain001DateSeq"+incomingPain001DateSeq);
		final String paymentService = "payments";
		final String paymentProduct = "sepa-credit-transfers";
		int printFactor = 1;
		if (logger.isInfoEnabled()) {
			logger.info("Incoming Pain001 XML Fields-------> " + xmlFileContent);
		}

		try {

			JSONObject xmlJSONObj = XML.toJSONObject(xmlFileContent);

			if (logger.isInfoEnabled()) {
				logger.info("Converted Incoming Pain001 XML to JSON Object-------> " + xmlJSONObj);
			}
			String convertedJSON = xmlJSONObj.toString(printFactor);

			if (logger.isInfoEnabled()) {
				logger.info("Converted JSON-------> " + convertedJSON);
			}
		
			JSONObject document = xmlJSONObj.getJSONObject("Document");
			
			
			JSONObject cstmrCdtTrfInitn = document.getJSONObject("CstmrCdtTrfInitn");
			JSONObject grpHdr = cstmrCdtTrfInitn.getJSONObject("GrpHdr");
			String msgId = grpHdr.get("MsgId").toString();

			UUID uuid = UUID.randomUUID();
			long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
			String uniqueGenMsgId =  Long.toString(l, Character.MAX_RADIX);
			
			if (logger.isInfoEnabled()) {
				logger.info("Entered inside JSON grp Header & Unique MsgId Created by Intellect is -------> " + uniqueGenMsgId);
			}

			JSONObject pmtInf = cstmrCdtTrfInitn.getJSONObject("PmtInf");

			String pmtInfId = pmtInf.get("PmtInfId").toString();

			JSONObject dbtrAgt = pmtInf.getJSONObject("DbtrAgt");
			JSONObject finInstnId = dbtrAgt.getJSONObject("FinInstnId");
			
			JSONObject cdtTrfTxInf = pmtInf.getJSONObject("CdtTrfTxInf");

			JSONObject dbtrAcct = pmtInf.getJSONObject("DbtrAcct");
			JSONObject DbtrAcctId = dbtrAcct.getJSONObject("Id");
			String debtoriBAN = DbtrAcctId.get("IBAN").toString();
			JSONObject debtorAccount = new JSONObject();
			debtorAccount.put("iban", debtoriBAN);

			System.out.println("Debtor Account Debtor IBAN from Incoming Pain001------->" + debtoriBAN);

			JSONObject CdtTrfTxInf = pmtInf.getJSONObject("CdtTrfTxInf");
			JSONObject cdtrAcct = CdtTrfTxInf.getJSONObject("CdtrAcct");
			JSONObject credAccId = cdtrAcct.getJSONObject("Id");
			String creditoriBAN = credAccId.get("IBAN").toString();
			JSONObject creditorAccount = new JSONObject();
			creditorAccount.put("iban", creditoriBAN);
			System.out.println("Creditor Account Creditor IBAN from Incoming Pain001------->" + creditoriBAN);

			JSONObject pmtId = cdtTrfTxInf.getJSONObject("PmtId");

			// Fetching & Mapping instructed Amount
			JSONObject amt = cdtTrfTxInf.getJSONObject("Amt");
			JSONObject instdAmt = amt.getJSONObject("InstdAmt");
			String ccy = instdAmt.get("Ccy").toString();
			String amount = instdAmt.get("content").toString();
			JSONObject instructedAmount = new JSONObject();
			instructedAmount.put("currency", ccy);
			instructedAmount.put("amount", amount);

			if (logger.isInfoEnabled()) {
				logger.info("Instructed Amount -------> " + instructedAmount);
			}

			JSONObject cdtr = cdtTrfTxInf.getJSONObject("Cdtr");
//			JSONObject ctctDtls = cdtr.getJSONObject("CtctDtls");
			if(cdtr.has("Nm")) {
			String nm = cdtr.get("Nm").toString();
			}
			// Fetching & Mapping endToEndIdentification
			String endToEndId = pmtId.get("EndToEndId").toString();
			
			// Fetching & Mapping Instruction Id
			
			String instrId = pmtId.get("InstrId").toString();
			
			List validIbanDtls = pisDao.validateIncomingIbanWithDB(url, userName, password);
			
			if (debtoriBAN.startsWith(IBOSConstants.HR)) {
				String ustrd ="Remittance Information";
				String accountHoldingBank = IBOSConstants.PBZ_BANK;
				JSONObject aHBJson = new JSONObject();
				aHBJson.put("instructedAmount", instructedAmount);
				// Fetching & Mapping Remittance information
				if(cdtTrfTxInf.has("RmtInf")) {
				JSONObject rmtInf = cdtTrfTxInf.getJSONObject("RmtInf");
				 ustrd = rmtInf.get("Ustrd").toString();

				if (logger.isInfoEnabled()) {
					logger.info("Remittance Information Unstructured -------> " + ustrd);
				}
				}
				aHBJson.put("remittanceInformationUnstructured", ustrd);
				//aHBJson.put("creditorName", nm);
				aHBJson.put("creditorName", "Creditor Name not available");
				aHBJson.put("endToEndIdentification", endToEndId);
				aHBJson.put("debtorAccount", debtorAccount);
				aHBJson.put("creditorAccount", creditorAccount);
				final Date date = new Date();
				final Random random = new Random();
				final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				String pbzPayId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
				aHBJson.put("pbzPayId", pbzPayId);
				aHBJson.put("ispPayId", "");
				
				if (logger.isInfoEnabled()) {
					logger.info("Constructed Input PBZ JSON from Incoming pain001 xml------->" + aHBJson);
				}

				// Converting to HashMap For hitting controller POST API
				/*
				 * HashMap<String, Object> aHBMap = new Gson().fromJson(aHBJson.toString(),
				 * HashMap.class); if (logger.isInfoEnabled()) {
				 * logger.info("Initiating Payment POST Req From Santander to PBZ Bank" +
				 * aHBMap); }
				 */

				if (validIbanDtls.contains(debtoriBAN)){
				Object ahbResponse = controller.paymentInitReqfromHBToAHB(paymentService, paymentProduct, aHBJson.toString(), null, null);
				pisDao.storePain001FromSantoAHB(pbzPayId, uniqueGenMsgId ,debtoriBAN, msgId, pmtInfId,endToEndId,instrId, url, userName, password);
				
				JSONObject fetchedPain001Dtls = pisDao.getPain001FromDBForPain002(pbzPayId, url, userName, password);
				String uniqueMsgId = fetchedPain001Dtls.get("uniqueGenMsgId").toString();
				String debtIban = fetchedPain001Dtls.get("debtoriBAN").toString();
				String OrgnlMsgId = fetchedPain001Dtls.get("msgId").toString();
				String status = fetchedPain001Dtls.get("status").toString();
				String pmtInfoId = fetchedPain001Dtls.get("pmtInfId").toString();
				String endToEndIdentification = fetchedPain001Dtls.get("endToEndId").toString();
				String instrIdentification = fetchedPain001Dtls.get("instrId").toString();
				if (logger.isInfoEnabled()) {
					logger.info("Persisting Pain001 Details");
				}
				if (logger.isInfoEnabled()) {
					logger.info("Converting to Pain002");
				}
				 
			String date_seq=pisDao.generatDateSequence(incomingPain001DateSeq);
			System.out.println("date_seq for PBZ "+date_seq);
 			pisDao.fileNameDateSequence(pbzPayId,url, userName, password);
 			pisDao.updatedDateSequence(pbzPayId,date_seq,url, userName, password);
 			String fetchedDateSeq=pisDao.fetchDate_Seq(pbzPayId,url, userName, password);
 			
				convertToPain002ACK(pbzPayId, uniqueMsgId, debtIban, OrgnlMsgId, "RCVD", pmtInfoId,fetchedDateSeq);
				if("ACSP".equals(status)||"PDNG".equals(status)||"RJCT".equals(status) && !"RCVD".equals(status)) {
				convertToPain002(pbzPayId,uniqueMsgId, debtIban, OrgnlMsgId, status,pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
				}else
				{
					status="PDNG";
					convertToPain002(pbzPayId,uniqueMsgId, debtIban, OrgnlMsgId, status,pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
				}
			}
				else {
					Object ahbResponse = controller.paymentInitReqfromHBToAHB(paymentService, paymentProduct, aHBJson.toString(), null, null);
					pisDao.storePain001FromSantoAHB(pbzPayId, uniqueGenMsgId ,debtoriBAN, msgId, pmtInfId,endToEndId,instrId, url, userName, password);
					String date_seq=pisDao.generatDateSequence(incomingPain001DateSeq);
					System.out.println("date_seq for PBZ "+date_seq);
					pisDao.fileNameDateSequence(pbzPayId,url, userName, password);
		 			pisDao.updatedDateSequence(pbzPayId,date_seq,url, userName, password);
		 			String fetchedDateSeq=pisDao.fetchDate_Seq(pbzPayId,url, userName, password);
		 			
					invalidIban(pbzPayId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,url,
							userName, password,fetchedDateSeq,endToEndId,instrId);		
					}

			} else if (debtoriBAN.startsWith(IBOSConstants.IT)) {
				String accountHoldingBank = IBOSConstants.ISP_BANK;

				JSONObject aHBJson = new JSONObject();
				aHBJson.put("instructedAmount", instructedAmount);
				//aHBJson.put("remittanceInformationUnstructured", ustrd);
				//aHBJson.put("creditorName", nm);
				aHBJson.put("creditorName", "Creditor Name not available");
//				JSONObject creditor = new JSONObject();
//				creditor.put("name", "Creditor Name not available");
//				aHBJson.put("creditor", creditor);
				aHBJson.put("endToEndIdentification", endToEndId);
				aHBJson.put("debtorAccount", debtorAccount);
				aHBJson.put("creditorAccount", creditorAccount);
				final Date date = new Date();
				final Random random = new Random();
				final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				String ispPayId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
				aHBJson.put("ispPayId", ispPayId);
				aHBJson.put("pbzPayId", "");
				

				// Converting to HashMap For hitting controller POST API
				/*
				 * HashMap<String, Object> aHBMap = new Gson().fromJson(aHBJson.toString(),
				 * HashMap.class);
				 * 
				 * if (logger.isInfoEnabled()) {
				 * logger.info("Initiating Payment POST Req From Santander to ISP Bank" +
				 * aHBMap); }
				 */
				if (validIbanDtls.contains(debtoriBAN)){
				Object ahbResponse = controller.paymentInitReqfromHBToAHB(paymentService, paymentProduct,
						aHBJson.toString(), null, null);
				pisDao.storePain001FromSantoAHB(ispPayId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,endToEndId,instrId, url, userName,
						password);
				JSONObject fetchedPain001Dtls = pisDao.getPain001FromDBForPain002(ispPayId, url, userName, password);

				if (logger.isInfoEnabled()) {
					logger.info("Fetched Pain001 Dtls From DB" + fetchedPain001Dtls);
				}

				String uniqueMsgId = fetchedPain001Dtls.get("uniqueGenMsgId").toString();
				String debtIban = fetchedPain001Dtls.get("debtoriBAN").toString();
				String OrgnlMsgId = fetchedPain001Dtls.get("msgId").toString();
				String status = fetchedPain001Dtls.get("status").toString();
				String pmtInfoId = fetchedPain001Dtls.get("pmtInfId").toString();
				String endToEndIdentification = fetchedPain001Dtls.get("endToEndId").toString();
				String instrIdentification = fetchedPain001Dtls.get("instrId").toString();

				String date_seq=pisDao.generatDateSequence(incomingPain001DateSeq);
				System.out.println("date_seq forISP"+date_seq);
				pisDao.fileNameDateSequence(ispPayId,url, userName, password);
	 			pisDao.updatedDateSequence(ispPayId,date_seq,url, userName, password);
	 			String fetchedDateSeq=pisDao.fetchDate_Seq(ispPayId,url, userName, password);
	 			System.out.println("fetchedDateSeq from fetchDate_Seq "+fetchedDateSeq);
				convertToPain002ACK(ispPayId, uniqueMsgId, debtIban, OrgnlMsgId, "RCVD", pmtInfoId,fetchedDateSeq);
				if("ACSP".equals(status)||"PDNG".equals(status)||"RJCT".equals(status)&& !"RCVD".equals(status)) {			
				convertToPain002(ispPayId, uniqueMsgId, debtIban, OrgnlMsgId, status, pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
				}else if(status.equals("ACCP")) {
					status="PDNG";
					convertToPain002(ispPayId, uniqueMsgId, debtIban, OrgnlMsgId, status, pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
				}
				else {
					status="PDNG";
					convertToPain002(ispPayId, uniqueMsgId, debtIban, OrgnlMsgId, status, pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
				}
				}else {
					Object ahbResponse = controller.paymentInitReqfromHBToAHB(paymentService, paymentProduct,
							aHBJson.toString(), null, null);
					pisDao.storePain001FromSantoAHB(ispPayId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,endToEndId,instrId, url, userName,
							password);

					String date_seq=pisDao.generatDateSequence(incomingPain001DateSeq);
					System.out.println("date_seq forISP"+date_seq);
					pisDao.fileNameDateSequence(ispPayId,url, userName, password);
		 			pisDao.updatedDateSequence(ispPayId,date_seq,url, userName, password);
		 			String fetchedDateSeq=pisDao.fetchDate_Seq(ispPayId,url, userName, password);
		 			System.out.println("fetchedDateSeq "+fetchedDateSeq);
					invalidIban(ispPayId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,url,
							userName, password,fetchedDateSeq,endToEndId,instrId);		
					}

			} else if (debtoriBAN.startsWith(IBOSConstants.FI)) {
				JSONObject sanToNordeaJSON = new JSONObject();
				String sanToNorAmount = instdAmt.get("content").toString();
				String sanToNorEndToEndId = pmtId.get("EndToEndId").toString();

				JSONObject debtorAcc = new JSONObject();
				JSONObject daccount = new JSONObject();
				daccount.put("value", debtoriBAN);
				daccount.put("type", "IBAN");
				daccount.put("currency", "EUR");

				debtorAcc.put("account", daccount);
				System.out.println(debtoriBAN + "Debtor:");

				JSONObject creditor = new JSONObject();
				JSONObject caccount = new JSONObject();
				caccount.put("value", creditoriBAN);
				caccount.put("type", "IBAN");
				//creditor.put("name", nm);
				creditor.put("name", "Creditor Name not available");
				creditor.put("account", caccount);
				
				if (cdtTrfTxInf.has("RmtInf")) {
					JSONObject rmtInf = cdtTrfTxInf.getJSONObject("RmtInf");
					String message = rmtInf.get("Ustrd").toString();
					creditor.put("message", message);
				} else
					creditor.put("message", "Remittance Information");

				sanToNordeaJSON.put("amount", sanToNorAmount);
				sanToNordeaJSON.put("currency", "EUR");
				sanToNordeaJSON.put("template_id", "SEPA_CREDIT_TRANSFER_FI");
				sanToNordeaJSON.put("currency", "EUR");
				sanToNordeaJSON.put("end_to_end_id", sanToNorEndToEndId);

				sanToNordeaJSON.put("authorizer_id", "authorizer_id");

				final Date date = new Date();
				final Random random = new Random();
				final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				String paymentId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
				sanToNordeaJSON.put("external_id", paymentId);
				sanToNordeaJSON.put("debtor", debtorAcc);
				sanToNordeaJSON.put("creditor", creditor);
				sanToNordeaJSON.put("pbzPayId", "");
				sanToNordeaJSON.put("ispPayId", "");

				/*
				 * HashMap<String, Object> sanToNordeaMap = new
				 * Gson().fromJson(sanToNordeaJSON.toString(), HashMap.class); String
				 * accountHoldingBank = IBOSConstants.NORDEA_BANK; if (logger.isInfoEnabled()) {
				 * logger.info("Initiating Payment POST Req From Santander to ISP Bank" +
				 * sanToNordeaMap); }
				 */

					if (validIbanDtls.contains(debtoriBAN)){
						Object ahbResponse = controller.paymentInitReqfromHBToAHB(paymentService, paymentProduct,
								sanToNordeaJSON.toString(), null, null);
						pisDao.storePain001FromSantoAHB(paymentId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,endToEndId,instrId, url,
								userName, password);
						JSONObject fetchedPain001Dtls = pisDao.getPain001FromDBForPain002(paymentId, url, userName,
								password);
						String uniqueMsgId = fetchedPain001Dtls.get("uniqueGenMsgId").toString();
						String debtIban = fetchedPain001Dtls.get("debtoriBAN").toString();
						String OrgnlMsgId = fetchedPain001Dtls.get("msgId").toString();
						String status = fetchedPain001Dtls.get("status").toString();
						String pmtInfoId = fetchedPain001Dtls.get("pmtInfId").toString();
						String endToEndIdentification = fetchedPain001Dtls.get("endToEndId").toString();
						String instrIdentification = fetchedPain001Dtls.get("instrId").toString();
						if (null != status) {
							if (status.equals("AUTHORIZATION_PENDING")) {
								status = "PDNG";
							} 
//							else if (status.equals("PAYMENT_EXECUTED")) {
//								status = "ACSC";
//							} 
							else if (status.equals("PAYMENT_REJECTED")) {
								status = "RJCT";
							} else if (status.equals("PAYMENT_ACCEPTED")) {
								status = "ACSP";
							}else
							{
								status = "PDNG";
							}
						}
						String date_seq=pisDao.generatDateSequence(incomingPain001DateSeq);
						System.out.println("date_seq for nordea"+date_seq);
						pisDao.fileNameDateSequence(paymentId,url, userName, password);
			 			pisDao.updatedDateSequence(paymentId,date_seq,url, userName, password);
			 			String fetchedDateSeq=pisDao.fetchDate_Seq(paymentId,url, userName, password);
                       
						convertToPain002ACK(paymentId, uniqueMsgId, debtIban, OrgnlMsgId, "RCVD", pmtInfoId,fetchedDateSeq);
						convertToPain002(paymentId, uniqueMsgId, debtIban, OrgnlMsgId, status, pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
					}else {
						Object ahbResponse = controller.paymentInitReqfromHBToAHB(paymentService, paymentProduct,
								sanToNordeaJSON.toString(), null, null);
						pisDao.storePain001FromSantoAHB(paymentId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,endToEndId,instrId, url,
								userName, password);
						String date_seq=pisDao.generatDateSequence(incomingPain001DateSeq);
						System.out.println("date_seq for nordea"+date_seq);
						pisDao.fileNameDateSequence(paymentId,url, userName, password);
			 			pisDao.updatedDateSequence(paymentId,date_seq,url, userName, password);
			 			String fetchedDateSeq=pisDao.fetchDate_Seq(paymentId,url, userName, password);

						invalidIban(paymentId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,url,
							userName, password,fetchedDateSeq,endToEndId,instrId);		
					}
			}else 
			{
				if (logger.isInfoEnabled()) {
					logger.info("Invalid IBAN -------------------" + debtoriBAN);
				}
				
				final Date date = new Date();
				final Random random = new Random();
				final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				String paymId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
				     pisDao.insertInvalidIban(paymId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,url,
								userName, password,endToEndId,instrId);
				String date_seq=pisDao.generatDateSequence(incomingPain001DateSeq);
				System.out.println("date_seq for invalid Iban"+date_seq);
				pisDao.fileNameDateSequence(paymId,url, userName, password);
	 			pisDao.updatedDateSequence(paymId,date_seq,url, userName, password);
	 			String fetchedDateSeq=pisDao.fetchDate_Seq(paymId,url, userName, password);
	 			System.out.println("fetchedDateSeq"+fetchedDateSeq);
				invalidIban(paymId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,url,
						userName, password,fetchedDateSeq,endToEndId,instrId);		
				
			}

		} catch (Exception e) {
			System.err.println("Error while Parsing Incoming Pain001 file" + e);

		}
	}

	private void invalidIban(String paymentId,String uniqueGenMsgId, String debtoriBAN, String msgId, String pmtInfId,
			String url, String userName, String password,String fetchedDateSeq,String endToEndId,String instrId) throws SQLException, ParserConfigurationException, TransformerException, IOException, SftpException {
		System.out.println("endToEndId is"+endToEndId+"and instrId +"+instrId +"fetchedDateSeq "+fetchedDateSeq);
		//pisDao.storeInvalidIbanPain001Dtls(paymentId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId, url,
			//	userName, password,"RJCT",endToEndId,instrId);
		
		pisDao.updatePain001FromSantoAHB(paymentId, uniqueGenMsgId, debtoriBAN, msgId, pmtInfId,endToEndId,instrId,"RJCT", url, userName,
				password);
	  JSONObject fetchedPain001Dtls = pisDao.getPain001FromDBForPain002(paymentId, url, userName,
				password);
		String uniqueMsgId = fetchedPain001Dtls.get("uniqueGenMsgId").toString();
		String debtIban = fetchedPain001Dtls.get("debtoriBAN").toString();
		String OrgnlMsgId = fetchedPain001Dtls.get("msgId").toString();
		String pmtInfoId = fetchedPain001Dtls.get("pmtInfId").toString();
		String status = fetchedPain001Dtls.get("status").toString();
		String endToEndIdentification = fetchedPain001Dtls.get("endToEndId").toString();
		String instrIdentification = fetchedPain001Dtls.get("instrId").toString();
		convertToPain002(paymentId, uniqueMsgId, debtIban, OrgnlMsgId, status, pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
		
	}

	private void convertToPain002ACK(String paymentId, String uniqueMsgId, String debtIban, String OrgnlMsgId,
			String status, String pmtInfoId,String fetchedDateSeq)
			throws ParserConfigurationException, TransformerException, IOException, SftpException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.newDocument();

		Element doc = document.createElement("Document");
		Attr xmlns = document.createAttribute("xmlns");
		xmlns.setValue("urn:iso:std:iso:20022:tech:xsd:pain.002.001.03");
		doc.setAttributeNode(xmlns);
		document.appendChild(doc);
		
		Element cstmrPmtStsRpt = document.createElement("CstmrPmtStsRpt");
		doc.appendChild(cstmrPmtStsRpt);
		Element grpHdr = document.createElement("GrpHdr");
		Element grpHdrMsgId = document.createElement("MsgId");
		grpHdrMsgId.appendChild(document.createTextNode(uniqueMsgId));
		grpHdr.appendChild(grpHdrMsgId);

		Element creDtTm = document.createElement("CreDtTm");
		Date currentDate = new Date();
		String sdf = pisDao.convertToGMTDate(currentDate, "YYYY-MM-dd'T'hh:mm:ss.sss", "GMT");
		creDtTm.appendChild(document.createTextNode(sdf));
		grpHdr.appendChild(creDtTm);

		cstmrPmtStsRpt.appendChild(grpHdr);
		
		Element orgnlGrpInfAndSts = document.createElement("OrgnlGrpInfAndSts");
		Element orgnlMsgId = document.createElement("OrgnlMsgId");
		orgnlMsgId.appendChild(document.createTextNode(OrgnlMsgId));
		orgnlGrpInfAndSts.appendChild(orgnlMsgId);

		Element orgnlMsgNmId = document.createElement("OrgnlMsgNmId");
		orgnlMsgNmId.appendChild(document.createTextNode("pain.001.001.03"));
		orgnlGrpInfAndSts.appendChild(orgnlMsgNmId);

		Element orgnlNbOfTxs = document.createElement("OrgnlNbOfTxs");
		orgnlNbOfTxs.appendChild(document.createTextNode("1"));
		orgnlGrpInfAndSts.appendChild(orgnlNbOfTxs);
		
		Element grpSts = document.createElement("GrpSts");
		grpSts.appendChild(document.createTextNode(status));
		orgnlGrpInfAndSts.appendChild(grpSts);

		cstmrPmtStsRpt.appendChild(orgnlGrpInfAndSts);

//		Element orgnlPmtInfAndSts = document.createElement("OrgnlPmtInfAndSts");
//		Element orgnlPmtInfId = document.createElement("OrgnlPmtInfId");
//		orgnlPmtInfId.appendChild(document.createTextNode(pmtInfoId));
//		orgnlPmtInfAndSts.appendChild(orgnlPmtInfId);
//		cstmrPmtStsRpt.appendChild(orgnlPmtInfAndSts);
//		
//		Element txInf = document.createElement("TxInfAndSts");
//		Element txSts = document.createElement("TxSts");
//		txSts.appendChild(document.createTextNode(status));
//		txInf.appendChild(txSts);
//		orgnlPmtInfAndSts.appendChild(txInf);
//		cstmrPmtStsRpt.appendChild(orgnlPmtInfAndSts);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String output = writer.getBuffer().toString();
		
        if (logger.isInfoEnabled()) {
			logger.info("Transformed ACK Pain002------------------->" + output );
		}
        
		String fileName = generateFileNameDtlACK(fetchedDateSeq);
		 if (logger.isInfoEnabled()) {
				logger.info("Transformed ACK Pain002 File Name------------------->" + fileName );
			}
		sendPain002ToSAN(fileName, ibosToSantander, output);

	}

	private String generateFileNameDtlACK(String fetchedDateSeq) {
		 //int num=1;
		System.out.println("generateFileNameDtlACK fetchedDateSeq"+fetchedDateSeq);
	      	
   	 // String var = String.format("%08d", num); 
   	  String temp = "IBOSIG" + "." + "FRK" + "." + "O" +  "." + "PAIN02"+  "." + "0003" +  "." + "SEPACT" +  "."+  "ACK"+
   	  "."+ fetchedDateSeq;
   				System.out.println(" File Name :" + temp);
	return temp;
   	
	}
	
	private void convertToPain002(String paymentId, String uniqueMsgId, String debtIban, String OrgnlMsgId, String status,String pmtInfoId
			,String endToEndIdentification,String instrIdentification,String fetchedDateSeq)
			throws ParserConfigurationException, TransformerException, IOException, SftpException, SQLException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.newDocument();

		Element doc = document.createElement("Document");
		Attr xmlns = document.createAttribute("xmlns");
		xmlns.setValue("urn:iso:std:iso:20022:tech:xsd:pain.002.001.03");
		doc.setAttributeNode(xmlns);
		document.appendChild(doc);
		
		Element cstmrPmtStsRpt = document.createElement("CstmrPmtStsRpt");
		doc.appendChild(cstmrPmtStsRpt);
		Element grpHdr = document.createElement("GrpHdr");
		Element grpHdrMsgId = document.createElement("MsgId");
		grpHdrMsgId.appendChild(document.createTextNode(uniqueMsgId));
		grpHdr.appendChild(grpHdrMsgId);

		Element creDtTm = document.createElement("CreDtTm");
		final Date date = new Date();

		Date currentDate = new Date();
		String sdf = pisDao.convertToGMTDate(currentDate, "YYYY-MM-dd'T'hh:mm:ss.sss", "GMT");
		creDtTm.appendChild(document.createTextNode(sdf));
		grpHdr.appendChild(creDtTm);

		cstmrPmtStsRpt.appendChild(grpHdr);
		
		Element orgnlGrpInfAndSts = document.createElement("OrgnlGrpInfAndSts");
		Element orgnlMsgId = document.createElement("OrgnlMsgId");
		orgnlMsgId.appendChild(document.createTextNode(OrgnlMsgId));
		orgnlGrpInfAndSts.appendChild(orgnlMsgId);

		Element orgnlMsgNmId = document.createElement("OrgnlMsgNmId");
		orgnlMsgNmId.appendChild(document.createTextNode("pain.001.001.03"));
		orgnlGrpInfAndSts.appendChild(orgnlMsgNmId);

		Element orgnlNbOfTxs = document.createElement("OrgnlNbOfTxs");
		orgnlNbOfTxs.appendChild(document.createTextNode("1"));
		orgnlGrpInfAndSts.appendChild(orgnlNbOfTxs);

		cstmrPmtStsRpt.appendChild(orgnlGrpInfAndSts);

		Element orgnlPmtInfAndSts = document.createElement("OrgnlPmtInfAndSts");
		Element orgnlPmtInfId = document.createElement("OrgnlPmtInfId");
		orgnlPmtInfId.appendChild(document.createTextNode(pmtInfoId));
		orgnlPmtInfAndSts.appendChild(orgnlPmtInfId);
		cstmrPmtStsRpt.appendChild(orgnlPmtInfAndSts);
		
		
		Element txInf = document.createElement("TxInfAndSts");
		Element orgnlInstrId = document.createElement("OrgnlInstrId");
		orgnlInstrId.appendChild(document.createTextNode(instrIdentification));
		txInf.appendChild(orgnlInstrId);
		
		Element orgnlEndToEndId = document.createElement("OrgnlEndToEndId");
		orgnlEndToEndId.appendChild(document.createTextNode(endToEndIdentification));
		txInf.appendChild(orgnlEndToEndId);
		
		
		Element txSts = document.createElement("TxSts");
		txSts.appendChild(document.createTextNode(status));
		txInf.appendChild(txSts);
		orgnlPmtInfAndSts.appendChild(txInf);
		cstmrPmtStsRpt.appendChild(orgnlPmtInfAndSts);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String output = writer.getBuffer().toString();
        
        if (logger.isInfoEnabled()) {
			logger.info("Transformed Pain002------------------->" + output );
		}
        
		String fileName = generateFileNameDtl(fetchedDateSeq);
		 if (logger.isInfoEnabled()) {
				logger.info("Transformed Pain002 File Name------------------->" + fileName );
			}
		sendPain002ToSAN(fileName, ibosToSantander, output);

	}

	
	private String generateFileNameDtl(String fetchedDateSeq) throws SQLException {
		// pisDao.createSeq(url,userName,password);
		System.out.println("generateFileNameDtl for FR"+fetchedDateSeq);
		//String num = pisDao.fileNameSequenceGenerator(url, userName, password);
		String temp = "IBOSIG" + "." + "FRK" + "." + "O" + "." + "PAIN02" + "." + "0003" + "." + "SEPACT" + "." + "FR"
				+ "." + fetchedDateSeq;
		System.out.println(" File Name :" + temp);
		return temp;

	}

	private void sendPain002ToSAN(String fileName, String ibosToSantander, String source)
			throws IOException, SftpException {
		SFTPConnectionUtilForPushingFiles lConnectionUtil =null;
		ChannelSftp lsftp = null;
		try {
			lConnectionUtil = new SFTPConnectionUtilForPushingFiles();
			lsftp = lConnectionUtil.getSFTPConnectionforPushingFiles();
			try {
			copyMessageToFile(fileName, source, lsftp);
			sendbkpfilesTomcatServer(fileName, source);
			}
			catch(Exception e ) {
				System.out.println("Exception occured while copying sendRejPain002ToSAN pain 002 to santander server "+e);
				copyMessagetoTomcatServer(fileName, source);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("SFTP Connection closed");
			//if (lsftp != null) {
				lConnectionUtil.closeConnection();
		//	}
		}

	}

	private void copyMessageToFile(String fileName, String source, ChannelSftp lsftp)
			throws SftpException, IOException {
		System.out.println("filePath::::" + ibosToSantander + " fileName:" + fileName);
		lsftp.cd(ibosToSantander);
		InputStream stream = new ByteArrayInputStream((source.getBytes()));
		lsftp.put(stream, ibosToSantander + fileName);
		stream.close();

	}
	
	private void sendbkpfilesTomcatServer(String fileName, String content) throws IOException {
		System.out.println("inside bkp folder for Pain02 ACK/FR");
	    BufferedWriter writer = new BufferedWriter(new FileWriter("/usr1/SIR21466/BackupFiles/"+fileName));
	    writer.write(content);
	    writer.close();		    
}
	private void copyMessagetoTomcatServer(String fileName, String content) throws IOException {
		
	    BufferedWriter writer = new BufferedWriter(new FileWriter(localPath+fileName));
	    writer.write(content);
	    writer.close();		    
}

}