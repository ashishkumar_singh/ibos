package com.intellect.ibos.Utils;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.intellect.ibos.DBConnectionUtil;
import com.intellect.ibos.controller.IbosMainController;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

/**
 * Scheduler for Payment Sender
 * 
 * @author nishanthi.mohan
 *
 */
@Component
public class PainSenderScheduler {

	@Value("${POST_PBZ_PAYMENT_ENDPOINT}")
	String pbzPaymentEndpoint;

	@Value("${DB_url}")
	String url;

	@Value("${DB_username}")
	String userName;

	@Value("${DB_password}")
	String password;
	
	@Value("${IBOS_TO_SAN}")
	private String ibosToSantander;
	
	
	@Value("${TOMCAT_PATH_FAILED_FILES}")
	String localPath;

	@Autowired
	IbosMainController controller;

	/**
	 * Auto wiring PisDao class
	 */

	PisDao pisDao = new PisDao();

	@Value("${Pain002_PATH}")
	private String pain002Path;

	/**
	 * Instantiating logger for logging purpose
	 */
	private static final Logger logger = LoggerFactory.getLogger(PainSenderScheduler.class);

	@Scheduled(fixedDelay = 900000)
	public void startProcessing()
			throws IOException, SftpException, ParserConfigurationException, SQLException, TransformerException {
		SFTPConnectionUtilForPushingFiles lConnectionUtil = new SFTPConnectionUtilForPushingFiles();
		ChannelSftp lsftp = null;
		DBConnectionUtil dbUtil = new DBConnectionUtil();
		Connection conn = null;
		try {
			lsftp = lConnectionUtil.getSFTPConnectionforPushingFiles();
			/*
			 * if (lsftp == null || lsftp.isClosed() || !lsftp.isConnected()) {
			 * System.out.println("SFTP Connection is not established"); return; } else { if
			 * (logger.isInfoEnabled()) { logger.info("SFTP Connection Established"); } }
			 */
			conn = dbUtil.mGetConnection(url, userName, password);
			if (conn == null || conn.isClosed()) {
				System.out.println("DB connection is not established");
				return;
			}
			sendGetStatusRequestToAhb(lsftp);

		} catch (IOException e) {
				System.out.println("Exception occured while sending pain messages"+e.getMessage());
			//e.printStackTrace();
		} finally {
			//if (lsftp != null) {
				lConnectionUtil.closeConnection();
			//}
			dbUtil.mCloseResources(null, conn);

		}
	}

	private void sendGetStatusRequestToAhb(ChannelSftp lsftp) throws SQLException, TransformerException, ParserConfigurationException, IOException, SftpException {
		
		List<String> pendingPayId = pisDao.getPendingStatusFromDB(url, userName, password);
		for (int i = 0; i < pendingPayId.size(); i++) {
			String paymentId = pendingPayId.get(i);
			if (logger.isInfoEnabled()) {
				logger.info("Payment ID with Pending Status to be Routed to AHB is -------------------" + paymentId);
			}
			controller.getSepaCreditStatusfromHBToAHB("payments", "sepa-credit-transfers",
					paymentId, null, null);
			JSONObject statusIbanDtls = pisDao.getStatusIbanFromDB(paymentId,url, userName, password);
			 if (statusIbanDtls.has("status") && statusIbanDtls.get("status").toString().equals("PAYMENT_ACCEPTED")
					 ||statusIbanDtls.get("status").toString().equals("ACSP")) {
					String statusDtls = "ACSP";
					pisDao.updateAHBStatus(statusDtls,paymentId,url, userName, password);
					JSONObject fetchedPain001Dtls = pisDao.getPain001FromDBForPain002(paymentId,url, userName, password);
					String uniqueMsgId = fetchedPain001Dtls.get("uniqueGenMsgId").toString();
					String debtIban = fetchedPain001Dtls.get("debtoriBAN").toString();
					String OrgnlMsgId = fetchedPain001Dtls.get("msgId").toString();
					String pmtInfoId = fetchedPain001Dtls.get("pmtInfId").toString();
					String endToEndIdentification = fetchedPain001Dtls.get("endToEndId").toString();
					String instrIdentification = fetchedPain001Dtls.get("instrId").toString();
					
					
		 		String fetchedDateSeq=pisDao.fetchDate_Seq(paymentId,url, userName, password);
					convertAHBToSanPain002(paymentId, uniqueMsgId, debtIban,OrgnlMsgId,statusDtls,pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
					logger.info("Payment Transaction Status is Completed");					
				}
			 else {
			if(statusIbanDtls.has("status") && statusIbanDtls.has("debtorIban")) {
			String status = statusIbanDtls.get("status").toString();
			System.out.println("statusIbanDtls status= "+status);
			String debtorIban = statusIbanDtls.get("debtorIban").toString();
			if(!status.equals("RJCT") && (debtorIban.startsWith(IBOSConstants.FI) ||debtorIban.startsWith(IBOSConstants.HR) || debtorIban.startsWith(IBOSConstants.IT))) {
			JSONObject fetchedPain001Dtls = pisDao.getPain001FromDBForPain002(paymentId,url, userName, password);
			if (logger.isInfoEnabled()) {
				logger.info("Fetched Pain001 Dtls From DB" + fetchedPain001Dtls);
			}
			if(null!= fetchedPain001Dtls && fetchedPain001Dtls.has("uniqueGenMsgId")&&fetchedPain001Dtls.has("debtoriBAN")&&
					fetchedPain001Dtls.has("msgId")&&fetchedPain001Dtls.has("status")
					&&fetchedPain001Dtls.has("pmtInfId")&&fetchedPain001Dtls.has("endToEndId")
					&&fetchedPain001Dtls.has("instrId")) {
				try {
			String uniqueMsgId = fetchedPain001Dtls.get("uniqueGenMsgId").toString();
			String debtIban = fetchedPain001Dtls.get("debtoriBAN").toString();
			String OrgnlMsgId = fetchedPain001Dtls.get("msgId").toString();
			String statusDtls = fetchedPain001Dtls.get("status").toString();
			String pmtInfoId = fetchedPain001Dtls.get("pmtInfId").toString();
			String endToEndIdentification = fetchedPain001Dtls.get("endToEndId").toString();
			String instrIdentification = fetchedPain001Dtls.get("instrId").toString();
			
					if (null != statusDtls) {
						if (statusDtls.equals("AUTHORIZATION_PENDING")) {
							statusDtls = "PDNG";
						} else if (statusDtls.equals("PAYMENT_EXECUTED")) {
							statusDtls = "ACSC";
						} else if (statusDtls.equals("PAYMENT_REJECTED")) {
							statusDtls = "RJCT";
						}else
						{
							statusDtls = "PDNG";
						}		
						pisDao.updateAHBStatus(statusDtls,paymentId,url, userName, password);
					}
			if (debtIban.startsWith(IBOSConstants.FI) ||debtIban.startsWith(IBOSConstants.HR) || debtIban.startsWith(IBOSConstants.IT)) {
			
				if(("ACSC".equals(statusDtls)||"PDNG".equals(statusDtls)||"RJCT".equals(statusDtls)) && !"RCVD".equals(statusDtls) && !"RCVI".equals(statusDtls)) {
			 		String fetchedDateSeq=pisDao.fetchDate_Seq(paymentId,url, userName, password);
				convertAHBToSanPain002(paymentId, uniqueMsgId, debtIban,OrgnlMsgId,statusDtls,pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
				}else
				{
					String fetchedDateSeq=pisDao.fetchDate_Seq(paymentId,url, userName, password);
					statusDtls="PDNG";
					convertAHBToSanPain002(paymentId, uniqueMsgId, debtIban,OrgnlMsgId,statusDtls,pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
				}
			}
			}catch (NullPointerException exp) {
				logger.info("Invalid Request------------------" + exp);
				throw exp;
				}
			}
		}else
		{
			String fetchedDateSeq=pisDao.fetchDate_Seq(paymentId,url, userName, password);
			JSONObject fetchedPain001Dtls = pisDao.getPain001FromDBForPain002(paymentId,url, userName, password);
			String uniqueMsgId = fetchedPain001Dtls.get("uniqueGenMsgId").toString();
			String debtIban = fetchedPain001Dtls.get("debtoriBAN").toString();
			String OrgnlMsgId = fetchedPain001Dtls.get("msgId").toString();
			String pmtInfoId = fetchedPain001Dtls.get("pmtInfId").toString();
			String endToEndIdentification = fetchedPain001Dtls.get("endToEndId").toString();
			String instrIdentification = fetchedPain001Dtls.get("instrId").toString();
			pisDao.updateAHBStatus("RJCT",paymentId,url, userName, password);
			generateRejectedPain002(paymentId, uniqueMsgId, debtIban,OrgnlMsgId,"RJCT",pmtInfoId,endToEndIdentification,instrIdentification,fetchedDateSeq);
					}
				}
			}

		}
	}
 
	private void generateRejectedPain002(String paymentId, String uniqueMsgId, String debtIban, String OrgnlMsgId,
			String statusDtls, String pmtInfoId,String endToEndIdentification,String instrIdentification,String fetchedDateSeq ) throws ParserConfigurationException, TransformerException, IOException, SftpException, SQLException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.newDocument();
		
		Element doc = document.createElement("Document");
		Attr xmlns = document.createAttribute("xmlns");
		xmlns.setValue("urn:iso:std:iso:20022:tech:xsd:pain.002.001.03");
		doc.setAttributeNode(xmlns);
		document.appendChild(doc);

		Element cstmrPmtStsRpt = document.createElement("CstmrPmtStsRpt");
		doc.appendChild(cstmrPmtStsRpt);
		Element grpHdr = document.createElement("GrpHdr");
		Element MsgId = document.createElement("MsgId");
		MsgId.appendChild(document.createTextNode(uniqueMsgId));
		grpHdr.appendChild(MsgId);
		cstmrPmtStsRpt.appendChild(grpHdr);

		Element creDtTm = document.createElement("CreDtTm");
		Date currentDate = new Date();
		String sdf = pisDao.convertToGMTDate(currentDate, "YYYY-MM-dd'T'hh:mm:ss.sss", "GMT");
		creDtTm.appendChild(document.createTextNode(sdf));
		grpHdr.appendChild(creDtTm);

		Element GrpInfAndSts = document.createElement("OrgnlGrpInfAndSts");
		Element orgnlMsgId = document.createElement("OrgnlMsgId");
		orgnlMsgId.appendChild(document.createTextNode(OrgnlMsgId));
		GrpInfAndSts.appendChild(orgnlMsgId);

		Element MsgNmId = document.createElement("OrgnlMsgNmId");
		MsgNmId.appendChild(document.createTextNode("pain.001.001.03"));
		GrpInfAndSts.appendChild(MsgNmId);
		cstmrPmtStsRpt.appendChild(GrpInfAndSts);

		Element OrgnlPmtInfAndSts = document.createElement("OrgnlPmtInfAndSts");
		Element PmtInfId = document.createElement("OrgnlPmtInfId");
		PmtInfId.appendChild(document.createTextNode(pmtInfoId));
		OrgnlPmtInfAndSts.appendChild(PmtInfId);
		
		Element txInf = document.createElement("TxInfAndSts");

		Element orgnlInstrId = document.createElement("OrgnlInstrId");
		orgnlInstrId.appendChild(document.createTextNode(instrIdentification));
		txInf.appendChild(orgnlInstrId);
		
		Element orgnlEndToEndId = document.createElement("OrgnlEndToEndId");
		orgnlEndToEndId.appendChild(document.createTextNode(endToEndIdentification));
		txInf.appendChild(orgnlEndToEndId);
		
		Element txSts = document.createElement("TxSts");
		txSts.appendChild(document.createTextNode("RJCT"));
		txInf.appendChild(txSts);
		
		OrgnlPmtInfAndSts.appendChild(txInf);
		cstmrPmtStsRpt.appendChild(OrgnlPmtInfAndSts);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String output = writer.getBuffer().toString();
      
        if (logger.isInfoEnabled()) {
			logger.info("Transformed Pain002------------------->" + output );
		}
	
		String fileName = generateFileNameDtl(fetchedDateSeq);
		 if (logger.isInfoEnabled()) {
				logger.info("Transformed Pain002 File Name------------------->" + fileName );
			}
		sendRejPain002ToSAN(fileName, ibosToSantander, output);
	}

	private void sendRejPain002ToSAN(String fileName, String ibosToSantander, String source) throws SftpException {
		SFTPConnectionUtilForPushingFiles lConnectionUtil = null;
		ChannelSftp lsftp = null;
		try {
			lConnectionUtil = new SFTPConnectionUtilForPushingFiles();
			lsftp = lConnectionUtil.getSFTPConnectionforPushingFiles();
			try {
			copyMessageToFile(fileName, source, lsftp);
			}
			catch(Exception e ) {
				System.out.println("Exception occured while copying sendRejPain002ToSAN pain 002 to santander server "+e);
				copyMessagetoTomcatServer(fileName, source);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("SFTP Connection closed");
				lConnectionUtil.closeConnection();
		}

	}

	private void convertAHBToSanPain002(String paymentId, String uniqueMsgId, String debtIban, String OrgnlMsgId, String status,String pmtInfoId,String endToEndIdentification,String instrIdentification,String fetchedDateSeq) throws ParserConfigurationException, TransformerException, IOException, SftpException, SQLException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.newDocument();
		
		Element doc = document.createElement("Document");
		Attr xmlns = document.createAttribute("xmlns");
		xmlns.setValue("urn:iso:std:iso:20022:tech:xsd:pain.002.001.03");
		doc.setAttributeNode(xmlns);
		document.appendChild(doc);

		Element cstmrPmtStsRpt = document.createElement("CstmrPmtStsRpt");
		doc.appendChild(cstmrPmtStsRpt);
		Element grpHdr = document.createElement("GrpHdr");
		Element MsgId = document.createElement("MsgId");
		MsgId.appendChild(document.createTextNode(uniqueMsgId));
		grpHdr.appendChild(MsgId);
		cstmrPmtStsRpt.appendChild(grpHdr);

		Element creDtTm = document.createElement("CreDtTm");
		Date currentDate = new Date();
		String sdf = pisDao.convertToGMTDate(currentDate, "YYYY-MM-dd'T'hh:mm:ss.sss", "GMT");
		creDtTm.appendChild(document.createTextNode(sdf));
		grpHdr.appendChild(creDtTm);

		Element GrpInfAndSts = document.createElement("OrgnlGrpInfAndSts");
		Element orgnlMsgId = document.createElement("OrgnlMsgId");
		orgnlMsgId.appendChild(document.createTextNode(OrgnlMsgId));
		GrpInfAndSts.appendChild(orgnlMsgId);

		cstmrPmtStsRpt.appendChild(GrpInfAndSts);
		
		Element MsgNmId = document.createElement("OrgnlMsgNmId");
		MsgNmId.appendChild(document.createTextNode("pain.001.001.03"));
		GrpInfAndSts.appendChild(MsgNmId);

		Element OrgnlPmtInfAndSts = document.createElement("OrgnlPmtInfAndSts");
		Element PmtInfId = document.createElement("OrgnlPmtInfId");
		PmtInfId.appendChild(document.createTextNode(pmtInfoId));
		OrgnlPmtInfAndSts.appendChild(PmtInfId);
		
		Element txInf = document.createElement("TxInfAndSts");
		
		Element orgnlInstrId = document.createElement("OrgnlInstrId");
		orgnlInstrId.appendChild(document.createTextNode(instrIdentification));
		txInf.appendChild(orgnlInstrId);
		
		Element orgnlEndToEndId = document.createElement("OrgnlEndToEndId");
		orgnlEndToEndId.appendChild(document.createTextNode(endToEndIdentification));
		txInf.appendChild(orgnlEndToEndId);
		
		Element txSts = document.createElement("TxSts");
		txSts.appendChild(document.createTextNode(status));
		txInf.appendChild(txSts);
		OrgnlPmtInfAndSts.appendChild(txInf);
		cstmrPmtStsRpt.appendChild(OrgnlPmtInfAndSts);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String output = writer.getBuffer().toString();
		
        if (logger.isInfoEnabled()) {
			logger.info("Transformed Pain002------------------->" + output );
		}
	
		String fileName = generateFileNameDtl(fetchedDateSeq);
		 if (logger.isInfoEnabled()) {
				logger.info("Transformed Pain002 File Name------------------->" + fileName );
			}
		sendPain002ToSAN(fileName, ibosToSantander, output);
	}

	

	private void sendPain002ToSAN(String fileName, String ibosToSantander, String source)
			throws IOException, SftpException {
		SFTPConnectionUtilForPushingFiles lConnectionUtil = null;
		ChannelSftp lsftp = null;
		try {
			lConnectionUtil = new SFTPConnectionUtilForPushingFiles();
			lsftp = lConnectionUtil.getSFTPConnectionforPushingFiles();
			try {
			copyMessageToFile(fileName, source, lsftp);
			sendbkpfilesTomcatServer(fileName, source);
			}
			catch(Exception e ) {
				System.out.println("Exception occured while copying pain 002 to santander server "+e);
				copyMessagetoTomcatServer(fileName, source);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("SFTP Connection closed");
		//	if (lsftp != null) {
				lConnectionUtil.closeConnection();
			//}
		}

	}
	private void sendbkpfilesTomcatServer(String fileName, String content) throws IOException {
		System.out.println("inside bkp folder for SET");
	    BufferedWriter writer = new BufferedWriter(new FileWriter("/usr1/SIR21466/BackupFiles/"+fileName));
	    writer.write(content);
	    writer.close();		    
}
	private void copyMessageToFile(String fileName, String source, ChannelSftp lsftp)
			throws SftpException, IOException {
		System.out.println("filePath::::" + ibosToSantander + " fileName:" + fileName);
		lsftp.cd(ibosToSantander);
		InputStream stream = new ByteArrayInputStream((source.getBytes()));
		lsftp.put(stream, ibosToSantander + fileName);
		stream.close();

	}

	private String generateFileNameDtl(String fetchedDateSeq) throws SQLException {
		//String num = pisDao.fileNameSequenceGenerator(url, userName, password);
		System.out.println("generateFileNameDtl fetchedDateSeq for SET" +fetchedDateSeq);
		String temp = "IBOSIG" + "." + "FRK" + "." + "O" + "." + "PAIN02" + "." + "0003" + "." + "SEPACT" + "." + "SET"
				+ "." + fetchedDateSeq;
		System.out.println(" File Name :" + temp);
		return temp;
	}
	
	
	private void copyMessagetoTomcatServer(String fileName, String content) throws IOException {
		
		    BufferedWriter writer = new BufferedWriter(new FileWriter(localPath+fileName));
		    writer.write(content);
		    writer.close();		    
	}
}