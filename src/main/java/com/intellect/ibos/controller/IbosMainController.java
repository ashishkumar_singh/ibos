package com.intellect.ibos.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;

import com.intellect.ibos.CommonUtil;
import com.intellect.ibos.Utils.GetFrameworkClient;
import com.intellect.ibos.Utils.IBOSConstants;
import com.intellect.ibos.Utils.PisDao;
import com.intellect.ibos.Utils.PostFrameworkClient;
import com.intellect.ibos.Utils.TokenGenerator;
import com.intellect.ibos.signverify.SignatureSigning;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Slf4j
@Component
@RequestMapping(path = "IBOS/v1")
public class IbosMainController {

	/**
	 * Auto wiring PisDao class
	 */
	@Autowired
	PisDao pisDao;// = new PisDao();

	@Autowired
	TokenGenerator tknObj;

	/**
	 * Instantiating logger for logging purpose
	 */
	private static final Logger logger = LoggerFactory.getLogger(IbosMainController.class);

	@Value("${GET_INTESSA_URI}")
	String intessaURI;

	@Value("${GET_INTESSA_ACCOUNT_ENDPOINT}")
	String intessaAccountEndpoint;

	@Value("${GET_PBZ_URI}")
	String pbzURI;

	@Value("${GET_PBZ_ACCOUNT_ENDPOINT}")
	String pbzAccountEndpoint;

	@Value("${GET_NORDEA_URI}")
	String nordeaURI;

	@Value("${GET_NORDEA_ACCOUNT_ENDPOINT}")
	String nordeaAccountEndpoint;

	@Value("${POST_NORDEA_PAYMENT_ENDPOINT}")
	String nordeaPaymentEndpoint;

	@Value("${POST_PBZ_PAYMENT_ENDPOINT}")
	String pbzPaymentEndpoint;

	@Value("${POST_ISP_PAYMENT_ENDPOINT}")
	String ispPaymentEndpoint;

	@Value("${GET_NORDEA_PAYMENT_STATUS_ENDPOINT}")
	String nordeaPaymentStatusEndpoint;

	@Value("${GET_PBZ_PAYMENT_STATUS_ENDPOINT}")
	String pbzPaymentStatusEndpoint;

	@Value("${GET_INTESA_PAYMENT_STATUS_ENDPOINT}")
	String intesaPaymentStatusEndpoint;

	@Value("${DB_url}")
	String url;

	@Value("${DB_username}")
	String userName;

	@Value("${DB_password}")
	String password;

	@GetMapping(value = "/accounts/{accountID}")
	public ResponseEntity<String> getAccountsfromIntellect(
			@RequestParam(value = "withBalance", required = true) String lWithBalance, @PathVariable String accountID,
			@RequestHeader(value = "Accept", required = true) String pAccept,
			@RequestHeader(value = "X-Request-ID", required = true) String pXRequestID,
			@RequestHeader(value = "Date", required = false) String pDate) {

		// GET
		// https://turmericpoc.intellectdesign.com/IBOS/v1/accounts/AB1232322121-EUR?withBalance=true\
		// Accept: application/json
		// X-Request-ID: 99391c7e-ad88-49ec-a2ad-99ddcb1f7721
		// Date: Thu, 08 Oct 2020 14:22:13 GMT

		String responseMesage = null;
		System.out.println("[getEnquiry]http request:{}" + accountID);
		ResponseEntity<String> response = null;
		String accountHoldingBank = "";
		CommonUtil util = new CommonUtil();
		try {
			System.out.println("message in processRefDataInquiryRequest request  : " + lWithBalance);
			
			// validate account ID from DB...
			String iban=null;
			if(accountID!=null) iban=accountID.replaceAll("_EUR", "");
			List validIbanDtls = pisDao.validateIncomingIbanWithDB(url, userName, password);
			if (iban!=null && validIbanDtls.contains(iban)){
			//if (accountID != null && (accountID.contains(IBOSConstants.FI) || accountID.contains(IBOSConstants.IT) || accountID.contains(IBOSConstants.HR) || accountID.contains(IBOSConstants.DE))) {

				// Identification of AHB Bank
				if (accountID.startsWith(IBOSConstants.FI))
					accountHoldingBank = IBOSConstants.NORDEA_BANK;
				if (accountID.startsWith(IBOSConstants.IT))
					accountHoldingBank = IBOSConstants.ISP_BANK;
				if (accountID.startsWith(IBOSConstants.HR))
					accountHoldingBank = IBOSConstants.PBZ_BANK;
				if (accountID.startsWith(IBOSConstants.DE))
					accountHoldingBank = IBOSConstants.SANTANDER_BANK;

				if (IBOSConstants.NORDEA_BANK.equalsIgnoreCase(accountHoldingBank)) {

					GetFrameworkClient client = new GetFrameworkClient();
					String pEndpoint = nordeaAccountEndpoint + accountID + "-EUR";
					Map tmp = getAccTknAuthID(accountID);
					if (tmp.get("accTkn")==null) {
						return returnError();
					}
					responseMesage = client.sendRequesForNordeaGetAccounts(nordeaURI, pEndpoint,
							tmp.get("accTkn").toString());
					if(responseMesage==null) {
						return returnBadReqErr();
					}
					System.out.println("Before -------------------" + responseMesage);
					responseMesage = util.convNortoIntessaJsonForAccounts(responseMesage).toString();
					System.out.println("After -------------------" + responseMesage);

					response = ResponseEntity.status(HttpStatus.OK).body(responseMesage);

				} else if (IBOSConstants.ISP_BANK.equalsIgnoreCase(accountHoldingBank)) {

					GetFrameworkClient client = new GetFrameworkClient();
					String pEndpoint = intessaAccountEndpoint + accountID + "/";
					responseMesage = client.sendRequesForIntessaPBZGetAccounts(intessaURI, pEndpoint, "true", accountHoldingBank);
					if(responseMesage==null) {
						return returnBadReqErr();
					}
					response = ResponseEntity.status(HttpStatus.OK).body(responseMesage);

				} else if (IBOSConstants.PBZ_BANK.equalsIgnoreCase(accountHoldingBank)) {

					GetFrameworkClient client = new GetFrameworkClient();

					String pEndpoint = pbzAccountEndpoint + accountID + "_EUR";
					responseMesage = client.sendRequesForIntessaPBZGetAccounts(pbzURI, pEndpoint, "true", accountHoldingBank);
					if(responseMesage==null) {
						return returnBadReqErr();
					}
					response = ResponseEntity.status(HttpStatus.OK).body(responseMesage);

				} else if (IBOSConstants.SANTANDER_BANK.equalsIgnoreCase(accountHoldingBank)) {

					// Convert Json from DB.....
					System.out.println(" new SimpleDateFormat(\"yyyy-MM-dd\").format(new Date()) "
							+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					System.out.println(" new SimpleDateFormat(\"yyyy-MM-dd\").format(new Date()) "
							+ new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					responseMesage = util
							.convSantoIntessaJsonForAccounts(new SimpleDateFormat("yyyy-MM-dd").format(new Date()),
									accountID, url, userName, password)
							.toString();

					response = ResponseEntity.status(HttpStatus.OK).body(responseMesage);
				}

			} else {
				response = ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("{\r\n" + "  \"Code\": \"" + "NOT_FOUND" + "\",\r\n" + "  \"Definition\": \""
								+ "Account Information is not currently accessible" + "\"\r\n" + "}");

				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.getBody());
			}

//			response = ResponseEntity.status(HttpStatus.OK).body("{\r\n" + 
//					"  \"AccountID\": \""+accountID+"\",\r\n" + 
//					"  \"WiithBalance\": \""+lWithBalance+"\"\r\n" + 
//					"}");

		} catch (HttpStatusCodeException ex) {
			ex.printStackTrace();
			response = ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders())
					.body(ex.getResponseBodyAsString());

			System.out.println("[getEnquiry]Response log:::{}" + response);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Unexpected exception received from Payment Processor");

		} catch (Exception exp) {
			exp.printStackTrace();
			response = ResponseEntity.status(500).body(exp.getMessage());
			System.out.println("[getEnquiry]Response log:::{}" + response);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Unexpectyed exception received from Payment Processor");
		}
		System.out.println("[getEnquiry]Response log:::{}" + response);

		return ResponseEntity.accepted().body(response.getBody());

	}

	public ResponseEntity<String> returnBadReqErr(){
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				             .body("{\r\n" + "  \"timestamp\": \"" + LocalDateTime.now() + "\",\r\n"  + "  \"status\": \"" + "400" + "\",\r\n" + "  \"error\": \""
						     + "Bad Request" + "\"\r\n" + "}");
	}
	
	public ResponseEntity<String> returnError(){
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				             .body("{\r\n" + "  \"Code\": \"" + "NOT_FOUND" + "\",\r\n" + "  \"Definition\": \""
						     + "Account Information is not currently accessible" + "\"\r\n" + "}");
	}
	
	@GetMapping(value = "/accounts/{accountID}/transactions")
	public ResponseEntity<String> getTransactionsFromIntellect(
			@RequestParam(value = "withBalance", required = false) String lWithBalance,
			@RequestParam(value = "dateFrom", required = true) @DateTimeFormat(pattern="yyyy-MM-dd") @Valid LocalDate dateFrom,
			@RequestParam(value = "dateTo", required = true) @DateTimeFormat(pattern="yyyy-MM-dd") @Valid LocalDate dateTo,
			@RequestParam(value = "bookingStatus", required = true) final String bookingStatus,
			@PathVariable String accountID, @RequestHeader(value = "Accept", required = true) String pAccept,
			@RequestHeader(value = "X-Request-ID", required = true) String pXRequestID,
			@RequestHeader(value = "Date", required = false) String pDate) {

		// GET
		// https://turmericpoc.intellectdesign.com/IBOS/v1/accounts/AB1232322121-EUR?withBalance=true\
		// Accept: application/json
		// X-Request-ID: 99391c7e-ad88-49ec-a2ad-99ddcb1f7721
		// Date: Thu, 08 Oct 2020 14:22:13 GMT

		String responseMesage = null;
		System.out.println("[getEnquiry]http request:{} transaction" + accountID);
		ResponseEntity<String> response = null;
		String accountHoldingBank = "";
		CommonUtil util = new CommonUtil();

		Map<String, String> queryParam = new HashMap<String, String>();
		queryParam.put("dateFrom", ""+dateFrom+"");
		queryParam.put("dateTo", ""+dateTo+"");
		queryParam.put("withBalance", lWithBalance);
		queryParam.put("bookingStatus", bookingStatus);

		try {
			System.out.println("message in processRefDataInquiryRequest request  : " + lWithBalance);

			String iban=null;
			if(accountID!=null) iban = accountID.replaceAll("_EUR", "");
			List validIbanDtls = pisDao.validateIncomingIbanWithDB(url, userName, password);
			if (iban!=null && validIbanDtls.contains(iban)){
			//if (accountID != null && (accountID.contains(IBOSConstants.FI) || accountID.contains(IBOSConstants.IT) || accountID.contains(IBOSConstants.HR) || accountID.contains(IBOSConstants.DE))) {

				// Identification of AHB Bank
				if (accountID.startsWith(IBOSConstants.FI))
					accountHoldingBank = IBOSConstants.NORDEA_BANK;
				if (accountID.startsWith(IBOSConstants.IT))
					accountHoldingBank = IBOSConstants.ISP_BANK;
				if (accountID.startsWith(IBOSConstants.HR))
					accountHoldingBank = IBOSConstants.PBZ_BANK;
				if (accountID.startsWith(IBOSConstants.DE))
					accountHoldingBank = IBOSConstants.SANTANDER_BANK;

				if (IBOSConstants.NORDEA_BANK.equalsIgnoreCase(accountHoldingBank)) {

					queryParam.put("from_date", ""+dateFrom+"");
					queryParam.put("to_date", ""+dateTo+"");

					GetFrameworkClient client = new GetFrameworkClient();
					String pEndpoint = nordeaAccountEndpoint + accountID + "-EUR" + "/transactions";

					Map tmp = getAccTknAuthID(accountID);
					if (tmp.get("accTkn")==null) {
						return returnError();
					}
					
					responseMesage = (String)client
							.sendRequesForNordeaGetTxn(nordeaURI, pEndpoint, queryParam, tmp.get("accTkn").toString());
					
					if(responseMesage=="" || responseMesage==null) {
						return returnBadReqErr();
					}
					
					System.out.println("Before -------------------" + responseMesage);
					responseMesage = util.convNortoIntessaJsonForTxn(responseMesage, accountID).toString();
					System.out.println("After -------------------" + responseMesage);
					HttpHeaders httpResponseHeaders = new HttpHeaders();
					httpResponseHeaders.set("X-Request-ID", pXRequestID);
					httpResponseHeaders.set("Date",
							new SimpleDateFormat("EEE, dd MMM yyyy HH:MM:SS zzz").format(new Date()));
					httpResponseHeaders.set("content-Type", "application/json");
					response = ResponseEntity.status(HttpStatus.OK).headers(httpResponseHeaders).body(responseMesage);

				} else if (IBOSConstants.ISP_BANK.equalsIgnoreCase(accountHoldingBank)) {

					GetFrameworkClient client = new GetFrameworkClient();
					String pEndpoint = intessaAccountEndpoint + accountID + "/transactions/";
					JSONObject jsonObj = client.sendRequesForIntessaPBZGetTxn(intessaURI, pEndpoint, queryParam, accountHoldingBank);
					if(jsonObj==null) {
						return returnBadReqErr();
					}
					response = ResponseEntity.status(HttpStatus.OK).body(jsonObj.toString());

				} else if (IBOSConstants.PBZ_BANK.equalsIgnoreCase(accountHoldingBank)) {

					GetFrameworkClient client = new GetFrameworkClient();

					String pEndpoint = pbzAccountEndpoint + accountID  + "_EUR" + "/transactions";
					JSONObject jsonObj = client.sendRequesForIntessaPBZGetTxn(pbzURI, pEndpoint, queryParam, accountHoldingBank);
					if(jsonObj==null) {
						return returnBadReqErr();
					}
					response = ResponseEntity.status(HttpStatus.OK).body(jsonObj.toString());

				} else if (IBOSConstants.SANTANDER_BANK.equalsIgnoreCase(accountHoldingBank)) {

					// Convert Json from DB.....

					responseMesage = util
							.convSantoIntessaJsonForTxn(accountID, ""+dateFrom+"", ""+dateTo+"", url, userName, password)
							.toString();

					response = ResponseEntity.status(HttpStatus.OK).body(responseMesage);

				}

			} else {
				response = ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body("{\r\n" + "  \"Code\": \"" + "NOT_FOUND" + "\",\r\n" + "  \"Definition\": \""
								+ "Account Information is not currently accessible" + "\"\r\n" + "}");

				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.getBody());
			}

//			response = ResponseEntity.status(HttpStatus.OK).body("{\r\n" + 
//					"  \"AccountID\": \""+accountID+"\",\r\n" + 
//					"  \"WiithBalance\": \""+lWithBalance+"\"\r\n" + 
//					"}");

		} catch (HttpStatusCodeException ex) {
			ex.printStackTrace();
			response = ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders())
					.body(ex.getResponseBodyAsString());

			System.out.println("[getEnquiry]Response log:::{}" + response);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Unexpected exception received from Payment Processor");

		} catch (Exception exp) {
			exp.printStackTrace();
			response = ResponseEntity.status(500).body(exp.getMessage());
			System.out.println("[getEnquiry]Response log:::{}" + response);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Unexpectyed exception received from Payment Processor");
		}
		System.out.println("[getEnquiry]Response log:::{}" + response);

		return ResponseEntity.accepted().body(response.getBody());

	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> Json2HashMapConverter(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> keysItr = object.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);
			// mPtLog.fatal("key:"+key+" value class :"+value.getClass());//1.9(Retro)
			if (value instanceof JSONObject) {
				value = Json2HashMapConverter((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	/**
	 * This End point will be used for payment Initiation request for the SEPA
	 * Credit Transfer from HB To AHB
	 * 
	 * @param paymentInitReq
	 * @param request
	 * @throws Exception 
	 */
	@PostMapping("/{paymentService}/{paymentProduct}")
	@ResponseStatus(value = HttpStatus.OK)
	public Object paymentInitReqfromHBToAHB(@PathVariable String paymentService, @PathVariable String paymentProduct,
			@RequestBody String paymentInitReq, 
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {
		String holdingBank = IBOSConstants.SANTANDER_BANK;
		String accountHoldingBank = "";
		String responseMesage = "";
		String SanToPbzPayId ="";
		String SanToIspPayId ="";
		ResponseEntity<String> responseEn = null;
		CommonUtil util = new CommonUtil();
		JSONObject paymentInitReqJson = new JSONObject(paymentInitReq);
		if(null == request) {
		if(!paymentInitReqJson.get("pbzPayId").equals("") || !paymentInitReqJson.get("ispPayId").equals("") ) {
		 SanToPbzPayId = paymentInitReqJson.get("pbzPayId").toString();
		 SanToIspPayId = paymentInitReqJson.get("ispPayId").toString();
		}
		paymentInitReqJson.remove("pbzPayId");
		paymentInitReqJson.remove("ispPayId");
		}
		if (logger.isInfoEnabled()) {
			logger.info("Input Json in IBOSMainContoller payment Init Req from HB To AHB -------------------"+ paymentInitReqJson);
		}
		
		if (null == request && holdingBank.equals(IBOSConstants.SANTANDER_BANK)) {
			String ibosId = "BSCHES";
			responseMesage = paymentInitReqfromSantanderToAHB(ibosId, paymentService, paymentProduct, paymentInitReqJson,
					request, response ,SanToPbzPayId ,SanToIspPayId).toString();
			return responseMesage;

		} else {
			if (null != request.getHeader("X-Request-ID") && !request.getHeader("X-Request-ID").isEmpty()) {
				final String xReqId = request.getHeader("X-Request-ID");
				if (logger.isInfoEnabled()) {
					logger.info("X-Request-ID-------------------" + xReqId);
				}
			} else {
				responseEn =  ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("{\"Code\": \"Bad Request\",  \"Definition\": \"x-request-id Mandatory\"}");
				return ResponseEntity.badRequest().body(responseEn.getBody());

			}
			if (null != request.getHeader("Content-Type") && !request.getHeader("Content-Type").isEmpty()) {
				final String contentType = request.getHeader("Content-Type");
				if (logger.isInfoEnabled()) {
					logger.info("Content-Type-------------------" + contentType);
				}
			} else {
				//return new ResponseEntity(HttpStatus.BAD_REQUEST);
				responseEn =  ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("{\"Code\": \"Bad Request\",  \"Definition\": \"Content-Type Mandatory\"}");
				return ResponseEntity.badRequest().body(responseEn.getBody());
			}

			/*
			 * if (null != request.getHeader("Date") &&
			 * !request.getHeader("Date").isEmpty()) { // To check if MANDATORY?? String
			 * reqDate = request.getHeader("Date"); if (logger.isInfoEnabled()) {
			 * logger.info("HB Request Date -------------------" + reqDate); } } else {
			 * return new ResponseEntity(HttpStatus.BAD_REQUEST); }
			 */
		}
		try {

			if (null != request) {
				String referrer = request.getHeader("referer");
				System.out.println("referrer:-" + referrer + "-----" + request.getRemoteAddr() + "\n1."
						+ request.getRemoteHost() + "\n2." + request.getRemoteUser() + "\n3."
						+ request.getRequestedSessionId() + "\n4." + request.getServerName());

				final String digest = request.getHeader("Digest");
				final String requestDate = request.getHeader("Date");
				final String xReqIdHB = request.getHeader("X-Request-ID");
				final String ibosId = request.getHeader("ibos-Id");
		        
				// Identification of HB Bank
				if (ibosId != null
						&& (ibosId.equalsIgnoreCase(IBOSConstants.HB_ISP) || ibosId.equals(IBOSConstants.HB_PBZ))) {
					if (ibosId.equalsIgnoreCase(IBOSConstants.HB_ISP))
						holdingBank = IBOSConstants.ISP_BANK;
					if (ibosId.equalsIgnoreCase(IBOSConstants.HB_PBZ))
						holdingBank = IBOSConstants.PBZ_BANK;
				}
				JSONObject debtorAccountObj = paymentInitReqJson.getJSONObject("debtorAccount");
				String debtorIban = debtorAccountObj.get("iban").toString();
				
				if (debtorIban != null && (debtorIban.contains(IBOSConstants.FI) || debtorIban.contains(IBOSConstants.IT)
						|| debtorIban.contains(IBOSConstants.HR) || debtorIban.contains(IBOSConstants.DE))) {			
				  if (IBOSConstants.ISP_BANK.equalsIgnoreCase(holdingBank)) {
					// Identification of AHB Bank
					if (debtorIban.startsWith(IBOSConstants.FI)) {
						accountHoldingBank = IBOSConstants.NORDEA_BANK;

						// Token Fetch / Generations/ Persist Process
						JSONObject debtorAccount = paymentInitReqJson.getJSONObject("debtorAccount");
						String debtorAcc = debtorAccount.get("iban").toString();
						Map rmap = getAccTknAuthID(debtorAcc);					
						if (rmap.get("accTkn")==null) {
							//throw new Exception ("Access Token generation exception");
							return returnError();
						}
						final Date date = new Date();
						final Random random = new Random();
						final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						String paymentId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
						JSONObject convertedISPToNordeaJson = pisDao.getPaymentInitReqFromISPToNordea(
								paymentInitReqJson, accountHoldingBank, rmap.get("authid").toString(), url, userName,
								password, paymentId, holdingBank);

						if (logger.isInfoEnabled()) {
							logger.info("Converted ISP To Nordea Json-------------------" + convertedISPToNordeaJson);
						}
						PostFrameworkClient postFrameworkClient = new PostFrameworkClient();
						String pEndpoint = nordeaPaymentEndpoint;
						if (logger.isInfoEnabled()) {
							logger.info("Nordea Payment POST Req End Point-------------------" + nordeaPaymentEndpoint);
						}
						if (logger.isInfoEnabled()) {
							logger.info("Intellect Generated PaymentId to Nordea---------------------" + paymentId);
						}
						
						if (logger.isInfoEnabled()) {
							logger.info("Input Request Json of Nordea------->ISP TO NORDEA<-------"+ convertedISPToNordeaJson ) ;
						}
						responseMesage = postFrameworkClient.sendNordRequest(convertedISPToNordeaJson.toString(),
								nordeaURI, pEndpoint, accountHoldingBank, request, rmap.get("accTkn").toString());
						JSONObject norResponse = new JSONObject(responseMesage);
						JSONObject grpHdr = norResponse.getJSONObject("group_header");
						String http_Code = grpHdr.get("http_code").toString();
						 if(null == responseMesage) {
							 return returnError();
							}  else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From Nordea-------------------" + responseMesage);
						}
							}
						responseMesage = pisDao
								.convNortoIntesaJsonForPay(responseMesage, paymentId, url, userName, password)
								.toString();
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From Nordea to Intesa-------------------" + responseMesage);
						}

					} else if (debtorIban.startsWith(IBOSConstants.HR)) {
						accountHoldingBank = IBOSConstants.PBZ_BANK;
						final Date date = new Date();
						final Random random = new Random();
						final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						String paymentId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
						pisDao.PersistPaymentInitReq(paymentId, paymentInitReqJson, accountHoldingBank, url, userName,
								password, holdingBank);
					
						PostFrameworkClient postFrameworkClient = new PostFrameworkClient();
						String pEndpoint = pbzPaymentEndpoint + '/' +paymentService + '/' + paymentProduct;
						if (logger.isInfoEnabled()) {
							logger.info("pbz Payment POST Req End Point-------------------------->" + pbzPaymentEndpoint);
						}
						if (logger.isInfoEnabled()) {
							logger.info("Intellect Generated PaymentId to PBZ--------------------->" + paymentId);
						}
						
						if (logger.isInfoEnabled()) {
							logger.info("Input Request Json of PBZ Bank ------->ISP TO PBZ<-------"+ paymentInitReqJson ) ;
						}
						responseMesage = postFrameworkClient.sendRequest(paymentInitReqJson.toString(), pbzURI,
								pEndpoint, accountHoldingBank, request);
						 if(null == responseMesage) {
							 //return ResponseEntity.status(HttpStatus.BAD_REQUEST);
							 return returnError();
							} else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From PBZ------------------->" + responseMesage);
						}
							}
						responseMesage = pisDao
								.convPBZtoISPJsonForPay(responseMesage, paymentId, url, userName, password).toString();
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From PBZ to Intesa---------->" + responseMesage);
						}
						Date dateDtl = java.util.Calendar.getInstance().getTime();
						final SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
						sdfDate.setTimeZone(TimeZone.getTimeZone("GMT"));
						response.setHeader("Date", sdfDate.format(dateDtl));
						response.setHeader("X-Request-ID", request.getHeader("X-Request-ID"));
						response.setHeader("Content-Type", "application/json");
						//CONFIRM LOCATION
						response.setHeader("Location", "https://intellectdesign.com/IBOS/v1/payments/sepa-credit-transfers/1234-wertiq-983");
					} else if (debtorIban.startsWith(IBOSConstants.DE)) {
						accountHoldingBank = IBOSConstants.SANTANDER_BANK;
						final Date date = new Date();
						final Random random = new Random();
						final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
						String paymentId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
						if (logger.isInfoEnabled()) {
							logger.info("PaymentId from ISP to Santander------------------->" + paymentId);
						}
						if (logger.isInfoEnabled()) {
							logger.info("Input Request of ISP to Santander----------------->"+ paymentInitReqJson ) ;
						}
						String Iban=""; 
						
						
						  if(paymentInitReqJson.has("debtorAccount")) 
						  {
							  JSONObject accountsFromjson = paymentInitReqJson.getJSONObject("debtorAccount");
							  
							  Iban =accountsFromjson.get("iban").toString();
							  System.out.println("IBAN of Santander when ISP is HB : : "+Iban); 
							  List validIbanDtls =pisDao.validateIncomingIbanWithDB(url, userName, password);
							  if(validIbanDtls.contains(Iban))
							  {
								  pisDao.PersistPaymentInitReq(paymentId, paymentInitReqJson, accountHoldingBank, url, userName,
										  	password, holdingBank);
								  	responseMesage = pisDao.generatePayIdresponse(paymentId).toString();
								  		if (logger.isInfoEnabled()) {
								  			logger.info("Generated PayId Response from Santander to ISP-------------------------->"+ responseMesage ) ;
								  					}
								  		pisDao.convertHBtoSanAHB(paymentInitReqJson, requestDate, paymentId, IBOSConstants.ISP_BANK);
							  }else
							  {
								  responseEn=ResponseEntity.status(HttpStatus.NOT_FOUND)
										.body("{\r\n" + "  \"Code\": \"" + "NOT_FOUND" + "\",\r\n" + "  \"Definition\": \""
												+ "Account Information is not currently accessible" + "\"\r\n" + "}");
								  return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseEn.getBody());
																		 
							  }
					 }

					}
				} else if (ibosId.equals(IBOSConstants.HB_PBZ)) {
					// Invoke signature / digest computation / validation
					SignatureSigning signa = new SignatureSigning();
					signa.verifysign(paymentInitReq, request); 
				    
					holdingBank = IBOSConstants.PBZ_BANK;
					if (IBOSConstants.PBZ_BANK.equalsIgnoreCase(holdingBank)) {
						// Identification of AHB Bank
						if (debtorIban.startsWith(IBOSConstants.FI)) {
							accountHoldingBank = IBOSConstants.NORDEA_BANK;

							// Token Fetch / Generations/ Persist Process
							JSONObject debtorAccount = paymentInitReqJson.getJSONObject("debtorAccount");
							String debtorAcc = debtorAccount.get("iban").toString();
							Map rmap = getAccTknAuthID(debtorAcc);
							if (rmap.get("accTkn")==null) {
								//throw new Exception ("Invalid IBAN");
								return returnError();
							}
							final Date date = new Date();
							final Random random = new Random();
							final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
							String paymentId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
							JSONObject convertedHBReqtoAhbReq = pisDao.getPaymentInitReqFromPBZToNordea(
									paymentInitReqJson, accountHoldingBank, url, userName, password, paymentId,
									holdingBank, rmap.get("authid").toString());

							PostFrameworkClient postFrameworkClient = new PostFrameworkClient();
							String pEndpoint = nordeaPaymentEndpoint;
							if (logger.isInfoEnabled()) {
								logger.info("Nordea Payment POST Req End Point------------------->" + pEndpoint);
							}
							
							if (logger.isInfoEnabled()) {
								logger.info("Input Request Json of Nordea Bank------->PBZ TO NORDEA<-------"+ convertedHBReqtoAhbReq);
							}
							
							responseMesage = postFrameworkClient.sendNordRequest(convertedHBReqtoAhbReq.toString(),
									nordeaURI, pEndpoint, accountHoldingBank, request, rmap.get("accTkn").toString());
							JSONObject norResponse = new JSONObject(responseMesage);
							JSONObject grpHdr = norResponse.getJSONObject("group_header");
							String http_Code = grpHdr.get("http_code").toString();
							 if(null == responseMesage || http_Code.equals("400")) {
								 	//throw new Exception ("BAD REQUEST");
								 return returnError();
								} else {
							if (logger.isInfoEnabled()) {
								logger.info("Response Message From Nordea------------------->" + responseMesage);
							}
								}
							responseMesage = pisDao
									.convNorToPBZJsonForPay(responseMesage, paymentId, url, userName, password)
									.toString();
							if (logger.isInfoEnabled()) {
								logger.info("Response Message From Nordea to PBZ------------------->" + responseMesage);
							}
						} else if (debtorIban.startsWith(IBOSConstants.IT)) {
							accountHoldingBank = IBOSConstants.ISP_BANK;
							final Date date = new Date();
							final Random random = new Random();
							final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
							String paymentId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
							pisDao.PersistPaymentInitReq(paymentId, paymentInitReqJson, accountHoldingBank, url,
									userName, password, holdingBank);
							PostFrameworkClient postFrameworkClient = new PostFrameworkClient();
							String pEndpoint = ispPaymentEndpoint + '/' + paymentService + '/' + paymentProduct;
							if (logger.isInfoEnabled()) {
								logger.info("ISP Req End Point------------------->" + pEndpoint);
							}
							
							if (logger.isInfoEnabled()) {
								logger.info("Input Request Json of PBZ Bank------->PBZ TO ISP<-------"+ paymentInitReqJson);
							}
							
							responseMesage = postFrameworkClient.sendRequest(paymentInitReqJson.toString(), intessaURI,
									pEndpoint, accountHoldingBank, request);
							if (null == responseMesage) {
								//return ResponseEntity.badRequest().body(responseMesage);
								 return returnError();
							} else {
							if (logger.isInfoEnabled()) {
								logger.info("Response Message From Intesa------------------->" + responseMesage);
							}
							}
							responseMesage = pisDao
									.convISPtoPBZJsonForPay(responseMesage, paymentId, url, userName, password)
									.toString();
							if (logger.isInfoEnabled()) {
								logger.info("Response Message From ISP to PBZ------------------->" + responseMesage);
							}
							Date dateDtl = java.util.Calendar.getInstance().getTime();
							final SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
							sdfDate.setTimeZone(TimeZone.getTimeZone("GMT"));
							response.setHeader("Date", sdfDate.format(dateDtl));
							response.setHeader("X-Request-ID", request.getHeader("X-Request-ID"));	
							response.setHeader("Content-Type", "application/json");
							response.setHeader("Location", "https://intellectdesign.com/IBOS/v1/payments/sepa-credit-transfers/1234-wertiq-983");
						
						} else if (debtorIban.startsWith(IBOSConstants.DE)) {
							accountHoldingBank = IBOSConstants.SANTANDER_BANK;
							final Date date = new Date();
							final Random random = new Random();
							final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
							String paymentId = sdf.format(date) + random.nextInt(Integer.MAX_VALUE);
							if (logger.isInfoEnabled()) {
								logger.info("PaymentId from PBZ to Santander------------------->" + paymentId);
							}
							if (logger.isInfoEnabled()) {
								logger.info("Input Request of PBZ to Santander----------------->"+ paymentInitReqJson ) ;
							}
							 String Iban=""; 
							  if(paymentInitReqJson.has("debtorAccount")) 
							  { 
								  JSONObject accountsFromjson = paymentInitReqJson.getJSONObject("debtorAccount");
								  Iban =accountsFromjson.get("iban").toString();
								  
							  System.out.println("IBAN of Santander when PBZ is HB : "+Iban); 
							  List validIbanDtls =pisDao.validateIncomingIbanWithDB(url, userName, password); 
							  if(validIbanDtls.contains(Iban))
							  {
								  pisDao.PersistPaymentInitReq(paymentId, paymentInitReqJson, accountHoldingBank, url,
										  userName, password, holdingBank);
								  pisDao.convertHBtoSanAHB(paymentInitReqJson, requestDate, paymentId,IBOSConstants.HB_PBZ);
								  if (logger.isInfoEnabled()) {
									  logger.info("Generated PayId Response from Santander to PBZ-------------------------->"+ responseMesage ) ;
								  	}
								  responseMesage = pisDao.generatePayIdresponse(paymentId).toString();
								  	if (logger.isInfoEnabled()) {
								  		logger.info("Response for Santander to PBZ----------------->"+ responseMesage ) ;
						     	}
							  }else
							  { 
								  responseEn=ResponseEntity.status(HttpStatus.NOT_FOUND)
											.body("{\r\n" + "  \"Code\": \"" + "NOT_FOUND" + "\",\r\n" + "  \"Definition\": \""
													+ "Account Information is not currently accessible" + "\"\r\n" + "}");
									  return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseEn.getBody());
								  
								}
					     }
						}
					}
				}
			 } else {
				 return returnError();
			   }
		   }
		} 
		/* catch (HttpStatusCodeException ex) {
			ex.printStackTrace();
			responseEn = ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders())
					.body(ex.getResponseBodyAsString());

			System.out.println("Response log1:::{}" + responseEn);
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Unexpected exception received from Payment Processor");
		} */
		catch (Exception exp) {
		    exp.printStackTrace();
			responseEn = ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"Code\": \"400 Bad Request\",  \"Definition\": \""+exp.getMessage()+"\"}");
			System.out.println("Response log2:::{}" + responseEn); 
			return responseEn;
			
		}
		return responseMesage;
	}

	private Object paymentInitReqfromSantanderToAHB(String ibosId, String paymentService, String paymentProduct,
			JSONObject paymentInitReqJson, HttpServletRequest request, HttpServletResponse response,String SanToPbzPayId, String SanToIspPayId)
			throws Exception {
		final String holdingBank = IBOSConstants.SANTANDER_BANK;
		String accountHoldingBank = "";
		String responseMesage = null;

		System.out.println("Entered Inside payment InitReq from Santander To AHB : payment Init Req Json is :"
				+ paymentInitReqJson.toString());

		String debtorIban = null;
		if (paymentInitReqJson.has("debtorAccount")) {
			JSONObject debtorAccountObj = paymentInitReqJson.getJSONObject("debtorAccount");
			if (debtorAccountObj.has("iban")) {
				debtorIban = debtorAccountObj.get("iban").toString();
				System.out.println("debtorAccountObj  debtorIban:" + debtorIban);
			}
		} else if (paymentInitReqJson.has("debtor")) {
			JSONObject debtorAccountObj = paymentInitReqJson.getJSONObject("debtor");
			JSONObject accountObj = debtorAccountObj.getJSONObject("account");
			debtorIban = accountObj.get("value").toString();
			System.out.println("Nordea debtorIban:" + debtorIban);

		}

		if (debtorIban.startsWith(IBOSConstants.FI)) {
			accountHoldingBank= IBOSConstants.NORDEA_BANK;
			if (logger.isInfoEnabled()) {
				logger.info("-------------------Routing Request from Santander to Nordea Bank-------------------");
			}
			String paymentId = paymentInitReqJson.get("external_id").toString();
			pisDao.getPaymentInitReqFromSanToNordea(accountHoldingBank, url, userName, password, paymentId,
					holdingBank);

			PostFrameworkClient postFrameworkClient = new PostFrameworkClient();
			String pEndpoint = nordeaPaymentEndpoint;
			if (logger.isInfoEnabled()) {
				logger.info("Nordea Payment POST End Point------------------->" + nordeaPaymentEndpoint);
			}

			System.out.println("Nordea debtorIban:" + debtorIban);
			Map rmap = getAccTknAuthID(debtorIban);
			if (rmap.get("accTkn")==null) {
				//throw new Exception ("Acces Token Generation Failed.");
				return returnError();
			}
			paymentInitReqJson.put("authorizer_id", rmap.get("authid").toString());

			if (logger.isInfoEnabled()) {
				logger.info("Input Request Json of Nordea Bank------->SAN TO NORDEA<-------"+ paymentInitReqJson);
			}
			responseMesage = postFrameworkClient.sendNordRequest(paymentInitReqJson.toString(), nordeaURI, pEndpoint,
					accountHoldingBank, request, rmap.get("accTkn").toString());JSONObject norResponse = new JSONObject(responseMesage);
					JSONObject grpHdr = norResponse.getJSONObject("group_header");
					String http_Code = grpHdr.get("http_code").toString();
					 if(null == responseMesage || http_Code.equals("400")) {
						 	//throw new Exception ("BAD REQUEST");
						 return returnError();
						}
			 else {
				if (logger.isInfoEnabled()) {
					logger.info("Response Message From Nordea------------------->" + responseMesage);
				}
			}
			responseMesage = pisDao.convNortoSanJsonForPay(responseMesage, paymentId, url, userName, password).toString();
			
			if (logger.isInfoEnabled()) {
				logger.info("Converted Nordea to San JSON------->NORDEA TO SAN<-------"+ responseMesage);
			}
		} else if (debtorIban.startsWith(IBOSConstants.HR)) {
			accountHoldingBank = IBOSConstants.PBZ_BANK;
			
			if (logger.isInfoEnabled()) {
				logger.info("-------------------Routing Request from Santander to PBZ Bank-------------------");
			}
			pisDao.PersistPaymentInitReq(SanToPbzPayId, paymentInitReqJson, accountHoldingBank, url, userName, password,
					holdingBank);
			PostFrameworkClient postFrameworkClient = new PostFrameworkClient();
			String pEndpoint = pbzPaymentEndpoint  + '/' + paymentService + '/' + paymentProduct;
			if (logger.isInfoEnabled()) {
				logger.info("PBZ Payment POST End Point------------------->" + pEndpoint);
				
				if (logger.isInfoEnabled()) {
					logger.info("Input Request Json of PBZ Bank------->SAN TO PBZ<-------"+ paymentInitReqJson);
				}
			}
			responseMesage = postFrameworkClient.sendRequest(paymentInitReqJson.toString(), pbzURI, pEndpoint,
					accountHoldingBank, request);
			if (null == responseMesage) {
				 //return ResponseEntity.status(HttpStatus.BAD_REQUEST);
				return returnError();
			} else {
			if (logger.isInfoEnabled()) {
				logger.info("Response Message From PBZ------------------->" + responseMesage);
			}
			}
			responseMesage = pisDao.convPBZToSanJsonForPay(responseMesage, SanToPbzPayId, url, userName, password).toString();
			if (logger.isInfoEnabled()) {
				logger.info("Converted PBZ to San JSON------->PBZ TO SAN<-------"+ responseMesage);
			}
		} else if (debtorIban.startsWith(IBOSConstants.IT)) {
			
			if (logger.isInfoEnabled()) {
				logger.info("-------------------Routing Request from Santander to ISP Bank-------------------");
			}
			accountHoldingBank = IBOSConstants.ISP_BANK;
			
			pisDao.PersistPaymentInitReq(SanToIspPayId, paymentInitReqJson, accountHoldingBank, url, userName, password,
					holdingBank);
			PostFrameworkClient postFrameworkClient = new PostFrameworkClient();
			String pEndpoint = ispPaymentEndpoint  + '/' + paymentService + '/' + paymentProduct;
			if (logger.isInfoEnabled()) {
				logger.info("ISP Payment POST End Point------------------->" + pEndpoint);
			}
		
			if (logger.isInfoEnabled()) {
				logger.info("Input Request Json of ISP Bank------->SAN TO ISP<-------"+ paymentInitReqJson);
			}
			responseMesage = postFrameworkClient.sendRequest(paymentInitReqJson.toString(), intessaURI, pEndpoint,
					accountHoldingBank, request);
			if (null == responseMesage) {
				return returnError();
			} else {
			if (logger.isInfoEnabled()) {
				logger.info("Response Message From Intesa------------------->" + responseMesage);
			}
			}
			pisDao.convISPtoSanJsonForPay(responseMesage, SanToIspPayId, url, userName, password);
		}

		return responseMesage;
	}

	/**
	 * This End point will be used to initiate get status request for the SEPA
	 * Credit Transfer from HB To AHB
	 * 
	 * @param paymentService
	 * @param paymentProd
	 * @param paymentId
	 * @throws SQLException
	 */
	@GetMapping(value = "/{paymentService}/{paymentProduct}/{paymentId}/status")
	public Object getSepaCreditStatusfromHBToAHB(@PathVariable String paymentService,
			@PathVariable String paymentProduct, @PathVariable String paymentId, HttpServletRequest request,
			HttpServletResponse response) throws SQLException {
		CommonUtil util = new CommonUtil();
		String responseMesage = "";
		String holdingBank = IBOSConstants.SANTANDER_BANK;
		HashMap ahmap = null;
		ResponseEntity<String> responseEn = null;
		if (null == request && holdingBank.equals(IBOSConstants.SANTANDER_BANK)) {
			responseMesage = getSepaCreditStatusfromSanToAHB(paymentService, paymentProduct, paymentId).toString();
		} else {
			if (null != request.getHeader("X-Request-ID") && !request.getHeader("X-Request-ID").isEmpty()) {
				final String xReqId = request.getHeader("X-Request-ID");
				if (logger.isInfoEnabled()) {
					logger.info("X-Request-ID-------------------" + xReqId);
				}
			} else {
				//return new ResponseEntity(HttpStatus.BAD_REQUEST);
				responseEn =  ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("{\"Code\": \"Bad Request\",  \"Definition\": \"x-request-id Mandatory\"}");
				return ResponseEntity.badRequest().body(responseEn.getBody());
			}
			if (null != request.getHeader("Content-Type") && !request.getHeader("Content-Type").isEmpty()) {
				final String contentType = request.getHeader("Content-Type");
				if (logger.isInfoEnabled()) {
					logger.info("Content-Type-------------------" + contentType);
				}
			} else {
				//return new ResponseEntity(HttpStatus.BAD_REQUEST);
				responseEn =  ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("{\"Code\": \"Bad Request\",  \"Definition\": \"Content-Type Mandatory\"}");
				return ResponseEntity.badRequest().body(responseEn.getBody());
			}

			/*
			 * if (null != request.getHeader("Date") &&
			 * !request.getHeader("Date").isEmpty()) { String reqDate =
			 * request.getHeader("Date"); if (logger.isInfoEnabled()) {
			 * logger.info("HB Request Date -------------------" + reqDate); } } else {
			 * return new ResponseEntity(HttpStatus.BAD_REQUEST); }
			 */
		}
		try {

			if (null != request) {
				final String idalPaymentId = paymentId;
				final String xReqId = request.getHeader("X-Request-ID");
				final String accept = request.getHeader("Accept");
				final String requestDate = request.getHeader("Date");
				// IDENTIFY AHB Based on the paymentId & HB FROM DB
				JSONObject ahbBankInfo = pisDao.IdentifyAHBFromDB(idalPaymentId, url, userName, password);
			//	if(ahbBankInfo.has("bankCode") && ahbBankInfo.has("ahbPayId")&& ahbBankInfo.has("hbBankCode")){
				String bankCode = ahbBankInfo.get("bankCode").toString();
				
				String hbBankCode = ahbBankInfo.get("hbBankCode").toString();
				System.out.println("]n bankCode:"+bankCode+"=hbBankCode:"+hbBankCode);
				// WHEN ISP -- NORDEA
				if ("ISP".equals(hbBankCode) && "NOR".equals(bankCode)) {
					GetFrameworkClient client = new GetFrameworkClient();
					String ahbPayId = ahbBankInfo.get("ahbPayId").toString();
					String pEndpoint = nordeaPaymentStatusEndpoint + '/' + ahbPayId;
					if (logger.isInfoEnabled()) {
						logger.info("Nordea Payment GET Req End Point------------------->" + nordeaPaymentEndpoint);
					}

					ahmap = new HashMap();
					ahmap = tknObj.accessTokenNordea("130474822427", "70311198");
					System.out.println("\n ++++++++++++accTkn::::::::" + ahmap.toString());

					responseMesage = client.sendRequestToNordeaFromISP(nordeaURI, pEndpoint,
							(String) ahmap.get("access_token"));
					JSONObject norResponse = new JSONObject(responseMesage);
					JSONObject grpHdr = norResponse.getJSONObject("group_header");
					String http_Code = grpHdr.get("http_code").toString();
					 if(null == responseMesage || http_Code.equals("400")) {
						 	throw new Exception ("BAD REQUEST");
						}else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From Nordea------->ISP TO NORDEA<-------" + responseMesage);
						}
					}

					responseMesage = pisDao.convNortoIntesaJsonForPayStatus(idalPaymentId,responseMesage,url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Converted Nordea to ISP JSON------->NORDEA TO ISP<------->GET STATUS<-------" + responseMesage);
					}
				} 
				// WHEN PBZ -- ISP
				else if ("PBZ".equals(hbBankCode) && "ISP".equals(bankCode)) {
					GetFrameworkClient client = new GetFrameworkClient();
					String ahbPayId = ahbBankInfo.get("ahbPayId").toString();
					String pEndpoint = intesaPaymentStatusEndpoint + "/" + paymentService + "/" + paymentProduct + "/"
							+ ahbPayId + "/" + "status";
					if (logger.isInfoEnabled()) {
						logger.info("Intesa Payment GET Req End Point------------------->" + pEndpoint);
					}
					responseMesage = client.sendRequestToISPFromPBZ(intessaURI, pEndpoint);
					if (null == responseMesage) {
						responseMesage="";
						return responseMesage;
					} else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From ISP------->PBZ TO ISP<-------" + responseMesage);
						}
					}
					responseMesage = pisDao.convIsptoPbzJsonForPayStatus(idalPaymentId,responseMesage,url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Converted ISP to PBZ JSON------->ISP TO PBZ<------->GET STATUS<-------" + responseMesage);
					}
					Date dateDtl = java.util.Calendar.getInstance().getTime();
					final SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
					sdfDate.setTimeZone(TimeZone.getTimeZone("GMT"));
					response.setHeader("Date", sdfDate.format(dateDtl));
					response.setHeader("X-Request-ID", request.getHeader("X-Request-ID"));
					response.setHeader("Content-Type", "application/json");
					response.setHeader("Location", "https://intellectdesign.com/IBOS/v1/payments/sepa-credit-transfers/1234-wertiq-983");
				

				} // WHEN ISP -- PBZ
				else if ("ISP".equals(hbBankCode) && "PBZ".equals(bankCode)) {
					GetFrameworkClient client = new GetFrameworkClient();
					String ahbPayId = ahbBankInfo.get("ahbPayId").toString();
					String pEndpoint = pbzPaymentStatusEndpoint + "/" + paymentService + "/" + paymentProduct + "/"
							+ ahbPayId + "/" + "status";
					if (logger.isInfoEnabled()) {
						logger.info("PBZ Payment GET Req End Point------------------->" + pEndpoint);
					}
					responseMesage = client.sendRequestToPBZFromISP(pbzURI, pEndpoint);
					if (null == responseMesage) {
						responseMesage="";
						return responseMesage;
					} else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From PBZ------->ISP TO PBZ<-------" + responseMesage);
						}
					}
					responseMesage = pisDao.convPbztoIspJsonForPayStatus(idalPaymentId,responseMesage,url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Converted PBZ to ISP JSON------->PBZ TO ISP<------->GET STATUS<-------" + responseMesage);
					}
					Date dateDtl = java.util.Calendar.getInstance().getTime();
					final SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
					sdfDate.setTimeZone(TimeZone.getTimeZone("GMT"));
					response.setHeader("Date", sdfDate.format(dateDtl));
					response.setHeader("X-Request-ID", request.getHeader("X-Request-ID"));
					response.setHeader("Content-Type", "application/json");
					response.setHeader("Location", "https://intellectdesign.com/IBOS/v1/payments/sepa-credit-transfers/1234-wertiq-983");
				
				}
				// PBZ TO NORDEA
				if ("PBZ".equals(hbBankCode) && "NOR".equals(bankCode)) {
					GetFrameworkClient client = new GetFrameworkClient();
					String ahbPayId = ahbBankInfo.get("ahbPayId").toString();
					String pEndpoint = nordeaPaymentStatusEndpoint + '/' + ahbPayId;
					if (logger.isInfoEnabled()) {
						logger.info("Nordea Payment GET Req End Point-------------------" + nordeaPaymentEndpoint);
					}

					ahmap = new HashMap();
					ahmap = tknObj.accessTokenNordea("130474822427", "70311198");
					System.out.println("\n ++++++++++++accTkn::::::::" + ahmap.toString());

					responseMesage = client.sendRequestToNordeaFromPbz(nordeaURI, pEndpoint,
							(String) ahmap.get("access_token"));
					JSONObject norResponse = new JSONObject(responseMesage);
					JSONObject grpHdr = norResponse.getJSONObject("group_header");
					String http_Code = grpHdr.get("http_code").toString();
					 if(null == responseMesage || http_Code.equals("400")) {
						 	throw new Exception ("BAD REQUEST");
						}else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From NORDEA------->PBZ TO NORDEA<-------" + responseMesage);
						}
					}
					responseMesage = pisDao.convNortoPbzJsonForPayStatus(idalPaymentId,responseMesage,url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Converted NORDEA to PBZ JSON------->NORDEA TO PBZ<------->GET STATUS<-------" + responseMesage);
					}
				}
				// PBZ---SAN
				else if ("PBZ".equals(hbBankCode) && "SAN".equals(bankCode)) {
					String txnstatus = pisDao.getPayStatusFromDB(idalPaymentId, url, userName, password)
							.toString();
					if (logger.isInfoEnabled()) {
						logger.info("Intellect Generated PaymentId-------------------" + idalPaymentId);
					}
					if (logger.isInfoEnabled()) {
						logger.info("Txn Status from DB for PBZ Request to Santander-------------------" + txnstatus);
					}
					responseMesage = pisDao.convSantoPBZJsonForPayStatus(idalPaymentId,txnstatus,url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Response Message From Santander to PBZ------------------->GET STATUS<-------" + responseMesage);
					}
				}
				// ISP---SAN
				else if ("ISP".equals(hbBankCode) && "SAN".equals(bankCode)) {
					String txnstatus = pisDao.getPayStatusFromDB(idalPaymentId, url, userName, password)
							.toString();
					if (logger.isInfoEnabled()) {
						logger.info(	"Intellect Generated PaymentId-------------------" + idalPaymentId);
					}
					if (logger.isInfoEnabled()) {
						logger.info(
								"Txn Status from DB for Intesa Request to Santander-------------------" + txnstatus);
					}
					responseMesage = pisDao.convSantoISPJsonForPayStatus(idalPaymentId,txnstatus,url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Response Message From Santander to ISP------------------->GET STATUS<-------" + responseMesage);
					}
				}
			//}
			}
		} 
		/*catch (Exception exp) {
			exp.printStackTrace();
			HashMap<String, Object> error = new HashMap<>();
			Date date = java.util.Calendar.getInstance().getTime();
			final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			error.put("Status", "HTTP/1.x  400 Bad Request");
			error.put("X-Request-ID", request.getHeader("X-Request-ID"));
			error.put("Date", sdf.format(date));
			error.put("Location",
					"https://intellectdesign.com/IBOS/v1/payments/sepa-credit-transfers/1234-wertiq-983");
			error.put("Content-Type", "application/json");
			error.put("payment_status_reason", "Payment instruction has been rejected");
			JSONObject errorResponse = new JSONObject(error);
			return errorResponse.toString();
		}*/
		catch (HttpStatusCodeException ex) {
			ex.printStackTrace();
			responseEn = ResponseEntity.status(ex.getRawStatusCode()).headers(ex.getResponseHeaders())
					.body(ex.getResponseBodyAsString());
			System.out.println("[getEnquiry]Response log:::{}" + response);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("Unexpected exception received from Payment Processor");

		} catch (Exception exp) {
			exp.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).
					body("{\"Code\": \"400 Bad Request\",  \"Definition\": \""+exp.getMessage()+"\"}");
					//body(exp.getMessage());
		}
		return responseMesage;

	}

	private Object getSepaCreditStatusfromSanToAHB(String paymentService, String paymentProduct, String paymentId)
			throws SQLException {
		final String holdingBank = IBOSConstants.SANTANDER_BANK;
		String responseMesage = "";
		Map ahmap = null;
		try {
			if (IBOSConstants.SANTANDER_BANK.equalsIgnoreCase(holdingBank)) {
				// IDENTIFY AHB Based on the paymentId
				final String idalPaymentId = paymentId;
				JSONObject ahbBankInfo = pisDao.IdentifyAHBFromDB(idalPaymentId, url, userName, password);
				if(ahbBankInfo.has("bankCode")&& ahbBankInfo.has("ahbPayId")) {
				String bankCode = ahbBankInfo.get("bankCode").toString();
				String ahbPayId = ahbBankInfo.get("ahbPayId").toString();

				// WHEN SAN -- NOR
				if ("NOR".equals(bankCode)) {
					if (logger.isInfoEnabled()) {
						logger.info("-------------------GET Request from Santander to Nordea Bank-------------------");
					}
					GetFrameworkClient client = new GetFrameworkClient();
					String pEndpoint = nordeaPaymentStatusEndpoint + '/' + ahbPayId;
					if (logger.isInfoEnabled()) {
						logger.info("Nordea Payment GET Req End Point------------------->" + nordeaPaymentEndpoint);
					}

					ahmap = new HashMap();
					ahmap = tknObj.accessTokenNordea("130474822427", "70311198");

					responseMesage = client.sendRequestToNordeaFromSan(nordeaURI, pEndpoint,
							(String) ahmap.get("access_token"));

					JSONObject norResponse = new JSONObject(responseMesage);
					JSONObject grpHdr = norResponse.getJSONObject("group_header");
					String http_Code = grpHdr.get("http_code").toString();
					 if(null == responseMesage || http_Code.equals("400")) {
						 	throw new Exception ("BAD REQUEST");
						} else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From NORDEA------->SAN TO NOR<------>GET STATUS<------" + responseMesage);
						}
					}
					responseMesage = pisDao.convNortoSanJsonForPayStatus(responseMesage, idalPaymentId, url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Converted NOR to SAN------->NOR TO SAN<-------" + responseMesage);
					}
				} // WHEN SAN -- ISP
				else if ("ISP".equals(bankCode)) {
					GetFrameworkClient client = new GetFrameworkClient();
					if (logger.isInfoEnabled()) {
						logger.info("-------------------GET Request from Santander to ISP Bank-------------------");
					}
					String pEndpoint = intesaPaymentStatusEndpoint + "/" + paymentService + "/" + paymentProduct + "/"
							+ ahbPayId + "/" + "status";
					if (logger.isInfoEnabled()) {
						logger.info("Intesa Payment GET Req End Point-------------------" + pEndpoint);
					}
					responseMesage = client.sendRequestToISPFromSan(intessaURI, pEndpoint);
					if (null == responseMesage) {
						responseMesage="";
						return responseMesage;
					} else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From ISP------->SAN TO ISP<------>GET STATUS<------" + responseMesage);
						}
					}
					responseMesage = pisDao.convIsptoSanJsonForPayStatus(responseMesage, idalPaymentId, url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Converted ISP to SAN------->ISP TO SAN<-------" + responseMesage);
					}
				} // WHEN SAN -- PBZ
				else if ("PBZ".equals(bankCode)) {
					GetFrameworkClient client = new GetFrameworkClient();
					if (logger.isInfoEnabled()) {
						logger.info("-------------------GET Request from Santander to PBZ Bank-------------------");
					}
					String pEndpoint = pbzPaymentStatusEndpoint + "/" + paymentService + "/" + paymentProduct + "/"
							+ ahbPayId + "/" + "status";
					if (logger.isInfoEnabled()) {
						logger.info("Pbz Payment GET Req End Point------------------->" + pEndpoint);
					}
					responseMesage = client.sendRequestToPBZFromSan(pbzURI, pEndpoint);
					if (null == responseMesage) {
						responseMesage="";
						return responseMesage;
					} else {
						if (logger.isInfoEnabled()) {
							logger.info("Response Message From PBZ------->SAN TO PBZ<------>GET STATUS<-----" + responseMesage);
						}
					}
					responseMesage = pisDao.convPbztoSanJsonForPayStatus(responseMesage, idalPaymentId, url, userName, password).toString();
					if (logger.isInfoEnabled()) {
						logger.info("Converted PBZ to SAN------->PBZ TO SAN<-------" + responseMesage);
					}
				}
			}}
		} catch (Exception exp) {
			exp.printStackTrace();
			HashMap<String, Object> error = new HashMap<>();
			Date date = java.util.Calendar.getInstance().getTime();
			final SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			error.put("Status", "HTTP/1.x  400 Bad Request");
			error.put("X-Request-ID", null);
			error.put("Date", sdf.format(date));
			error.put("Location",
					"https://intellectdesign.com/IBOS/v1/payments/sepa-credit-transfers/1234-wertiq-983");
			error.put("Content-Type", "application/json");
			error.put("payment_status_reason", "Payment instruction has been rejected");
			JSONObject errorResponse = new JSONObject(error);
			return errorResponse.toString();
		}
		return responseMesage;
	}

	public Map getAccTknAuthID (String debtorAcc) throws Exception {
		String accTkn = null, authid = null, agreementid = null;
		int datediff = 0;
		Map hmaps = null;
		Map ahmaps = null;
		Map<String, String> resultMap = null;
		System.out.println("\n ++debtorAcc::" + debtorAcc);

		try {
			hmaps = pisDao.getAccessTokenInfo(debtorAcc, url, userName, password);
			resultMap = new HashMap<String, String>();
			if (hmaps != null && !hmaps.isEmpty()) {
				agreementid = (String) hmaps.get("agreementid");
				authid = (String) hmaps.get("authid");
				datediff = (int) hmaps.get("datediff");
				accTkn = (String) hmaps.get("accessKey");
				resultMap.put("authid", authid);
				resultMap.put("accTkn", accTkn);

				if (datediff < 1500 && accTkn != null) {
					System.out.println("\n ++++++Use Same Old accessTkn:");
				} else {
					System.out.println("\n +++++Create New accessTkn & Update:");
					ahmaps = tknObj.accessTokenNordea(agreementid, authid);
					accTkn = (String) ahmaps.get("access_token");
					resultMap.put("accTkn", accTkn);
					pisDao.updateAuthAgreementId(accTkn, (String) ahmaps.get("creation_time"), agreementid, authid, url,
							userName, password);
				}
			}
			System.out.println("\n ++++++accessKey:\n" + accTkn);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Invalid IBAN Data");// checked exception
		}
		return resultMap;

	}
}
