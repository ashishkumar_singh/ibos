/**
 * These materials are confidential and proprietary to Intellect Design Arena Ltd. and no part of
 * these materials should be reproduced, published, transmitted or distributed in any form or by any
 * means, electronic, mechanical, photocopying, recording or otherwise, or stored in any information
 * storage or retrieval system of any nature nor should the materials be disclosed to third parties
 * or used in any other manner for which this is not authorized, without the prior express written
 * authorization of Intellect Design Arena Ltd.
 *
 * <p>Copyright 2019 Intellect Design Arena Limited. All rights reserved.
 */
/**
 * ******************************************************************* Version Control Block
 *
 * <p>Date Version Author Description --------- -------- --------------- ---------------------------
 * 
 */
package com.intellect.ibos;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.intellect.ibos.Utils.IBOSConstants;
import com.intellect.ibos.Utils.MTXXXDao;


@Component
public class CommonUtil {	
	
	public JSONObject convNortoIntessaJsonForAccounts(String AHBresultMapString) {
		
		JSONArray balancesforIntessaAndPBZ = new JSONArray();		    
		JSONObject accountForIntesaandPBZ = new JSONObject();
				
	    try {
	      Field changeMap = accountForIntesaandPBZ.getClass().getDeclaredField("map");
	      changeMap.setAccessible(true);
	      changeMap.set(accountForIntesaandPBZ, new LinkedHashMap<>());
	      changeMap.setAccessible(false);
	    } catch (IllegalAccessException | NoSuchFieldException e) {
	    	System.out.println(e.getMessage());
	    }
	    
		JSONObject responseFromNordea  = new JSONObject(AHBresultMapString);

		try {
			JSONObject resultMap = (JSONObject) responseFromNordea.get("response");
			
			System.out.println("restul Map :"+resultMap);
			
			accountForIntesaandPBZ.put(IBOSConstants.account, resultMap.get(IBOSConstants.iban));

			//IBan
			accountForIntesaandPBZ.put(IBOSConstants.iban, resultMap.get(IBOSConstants.iban));
		
			//Currency
			accountForIntesaandPBZ.put(IBOSConstants.currency, resultMap.get(IBOSConstants.currency));
			
			if(AHBresultMapString.contains(IBOSConstants.account_short_name)) {
				//account short name
				accountForIntesaandPBZ.put(IBOSConstants.ownerName, resultMap.get(IBOSConstants.account_short_name));
			}
			
			if(AHBresultMapString.contains(IBOSConstants.account_name)) {
				//account name
				accountForIntesaandPBZ.put(IBOSConstants.name, resultMap.get(IBOSConstants.account_name));
			}
			
			//status
			if(AHBresultMapString.contains("status")) {
				String status= resultMap.get(IBOSConstants.status).toString();
				if("OPEN".equalsIgnoreCase(status)) {
					accountForIntesaandPBZ.put(IBOSConstants.status, "enabled");
				} else if ("CLOSED".equalsIgnoreCase(status)){
					accountForIntesaandPBZ.put(IBOSConstants.status, "deleted"); 
				}
			}
			
			//population of balances
			
			
//			- If available_balance, then BalanceType=forwardAvailable
//			- If booked_balance, then BalanceType=closingBooked
//			- If value_dated_balance, then BalanceType=openingBooked"
			
			if(AHBresultMapString.contains("value_dated_balance")) {
				JSONObject tempObj = new JSONObject();
				tempObj.put(IBOSConstants.referenceDate,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				tempObj.put(IBOSConstants.balanceType,"forwardAvailable");
				
				JSONObject balanceAmount = new JSONObject();
				balanceAmount.put(IBOSConstants.amount,resultMap.get("value_dated_balance"));
				balanceAmount.put(IBOSConstants.currency, resultMap.get(IBOSConstants.currency));
				
				tempObj.put(IBOSConstants.balanceAmount,balanceAmount);
				
				if(AHBresultMapString.contains("latest_transaction_booking_date")) {
					tempObj.put("lastChangedDateTime",resultMap.get("latest_transaction_booking_date"));
					}
				
				//balancesforIntessaAndPBZ.put(tempObj);
			}

			if(AHBresultMapString.contains("available_balance")) {
				JSONObject tempObj = new JSONObject();
				tempObj.put(IBOSConstants.balanceType,"openingBooked");
				
				JSONObject balanceAmount = new JSONObject();
				balanceAmount.put(IBOSConstants.amount,resultMap.get("available_balance"));
				balanceAmount.put(IBOSConstants.currency, resultMap.get(IBOSConstants.currency));
				
				tempObj.put(IBOSConstants.balanceAmount,balanceAmount);
				tempObj.put(IBOSConstants.referenceDate,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

				/*if(AHBresultMapString.contains("latest_transaction_booking_date")) {
					tempObj.put("lastChangedDateTime",resultMap.get("latest_transaction_booking_date"));
					}
				*/
				
				System.out.println(" tempObj : "+ tempObj);

				balancesforIntessaAndPBZ.put(tempObj);
			}
			
			if(AHBresultMapString.contains("booked_balance")) {
				JSONObject tempObj = new JSONObject();
				//tempObj.put(IBOSConstants.referenceDate,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				tempObj.put(IBOSConstants.balanceType,"closingBooked");
				
				JSONObject balanceAmount = new JSONObject();
				balanceAmount.put(IBOSConstants.amount,resultMap.get("booked_balance"));
				balanceAmount.put(IBOSConstants.currency, resultMap.get(IBOSConstants.currency));
				
				tempObj.put(IBOSConstants.balanceAmount,balanceAmount);
				
				if(AHBresultMapString.contains("latest_transaction_booking_date")) {
					tempObj.put("lastChangedDateTime",resultMap.get("latest_transaction_booking_date"));
					}
				
				balancesforIntessaAndPBZ.put(tempObj);
			}
			
			accountForIntesaandPBZ.put(IBOSConstants.balances, balancesforIntessaAndPBZ);
			System.out.println(" accountForIntesaandPBZ : "+ accountForIntesaandPBZ);
				
			
		}
		catch (Exception e){
			e.getLocalizedMessage();
			e.printStackTrace();
			accountForIntesaandPBZ = new JSONObject("{\r\n" + 
					"  \"Code\": \""+"ERROR_OCCURED"+"\",\r\n" + 
					"  \"Definition\": \""+"Error occurred while accessing account information. The details cannot be returned right now"+"\"\r\n" + 
					"}");
		}
		
		
		return accountForIntesaandPBZ;
	}
	
	
	public JSONObject convNortoIntessaJsonForTxn(String AHBresultMapString,String accountID) {
		
		JSONObject intessaPBZResponseForTxn = new JSONObject();
		JSONObject resultMap = new JSONObject(AHBresultMapString);
		JSONObject accountForIntesaandPBZ = new JSONObject();
		
		try {
		System.out.println("Entering Convertor of transactions from Nordea to JSON");
			
			//IBan
		accountForIntesaandPBZ.put(IBOSConstants.iban, accountID);
//		accountForIntesaandPBZ.put("currency", value);
		intessaPBZResponseForTxn.put(IBOSConstants.account, accountForIntesaandPBZ);
		
		//For Transactions
		JSONObject responseFromNordea = (JSONObject) resultMap.get("response");
		JSONArray  transactionsFromNordea = (JSONArray) responseFromNordea.get("transactions");
		
		JSONArray bookedIntessaTransactions  = new JSONArray();
		JSONArray PendingIntessaTransactions  = new JSONArray();
		JSONObject transactionsFromIntessa = new JSONObject();
		
		
		for(Object tempObject : transactionsFromNordea) {
			JSONObject nordeaTransaction = (JSONObject) tempObject;
			JSONObject intessaPBZTransaction=new JSONObject();

			intessaPBZTransaction.put("transactionId", nordeaTransaction.get("transaction_id"));
			intessaPBZTransaction.put("bookingDate", nordeaTransaction.get("booking_date"));
			intessaPBZTransaction.put("valueDate", nordeaTransaction.get("value_date"));
			JSONObject transactionAmount = new JSONObject();
			transactionAmount.put("amount",nordeaTransaction.get("amount"));
			if(nordeaTransaction.has("currency")) {
				transactionAmount.put("currency",nordeaTransaction.get("currency"));
				accountForIntesaandPBZ.put("currency", nordeaTransaction.get("currency"));
			}
			intessaPBZTransaction.put("transactionAmount", transactionAmount);
			//intessaPBZTransaction.put("debtorName", accountForIntesaandPBZ);
			
			
			
			
			
			if(nordeaTransaction.has("narrative"))intessaPBZTransaction.put("additionalInformation", nordeaTransaction.get("narrative"));
              System.out.println("intessaPBZTransaction.get(\"transactionAmount\").toString()"+intessaPBZTransaction.get("transactionAmount").toString());
			if(intessaPBZTransaction.has("transactionAmount") && intessaPBZTransaction.get("transactionAmount").toString().contains("-")) {
				if(nordeaTransaction.has("counterparty_name"))intessaPBZTransaction.put("debtorName", nordeaTransaction.get("counterparty_name"));
				intessaPBZTransaction.put("debtorAccount", accountForIntesaandPBZ);
				PendingIntessaTransactions.put(intessaPBZTransaction);
			}
			else {
				if(nordeaTransaction.has("counterparty_name"))intessaPBZTransaction.put("creditorName", nordeaTransaction.get("counterparty_name"));
				intessaPBZTransaction.put("creditorAccount", accountForIntesaandPBZ);
				bookedIntessaTransactions.put(intessaPBZTransaction);
			}
			
			System.out.println(" intessaPBZTransaction   "+intessaPBZTransaction);
			
			
		}
		
		transactionsFromIntessa.put("booked", bookedIntessaTransactions);
		transactionsFromIntessa.put("pending", PendingIntessaTransactions);
		
		intessaPBZResponseForTxn.put("transactions", transactionsFromIntessa);
		
		System.out.println("intessaPBZResponseForTxn :"+ intessaPBZResponseForTxn);
		}
		catch(Exception e) {
			e.printStackTrace();
			intessaPBZResponseForTxn = new JSONObject("{\r\n" + 
					"  \"Code\": \""+"ERROR_OCCURED"+"\",\r\n" + 
					"  \"Definition\": \""+"Error occurred while accessing account information. The details cannot be returned right now \"\r\n" + 
					"}");
		}
		
		return intessaPBZResponseForTxn;
		
	}
	
	
	public JSONObject convSantoIntessaJsonForAccounts(String accountDate,String accountID, String url, String userName, String password) {
		
		System.out.println(" accountID :"+ accountID +" accountFromDate : "+accountDate);
		
		JSONArray balancesforIntessaAndPBZ = new JSONArray();
		JSONObject accountForIntesaandPBZ = new JSONObject();
		
		try {
		      Field changeMap = accountForIntesaandPBZ.getClass().getDeclaredField("map");
		      changeMap.setAccessible(true);
		      changeMap.set(accountForIntesaandPBZ, new LinkedHashMap<>());
		      changeMap.setAccessible(false);
		    } catch (IllegalAccessException | NoSuchFieldException e) {
		    	System.out.println(e.getMessage());
		    }
		
		DBConnectionUtil dbUtil = new DBConnectionUtil();
		Connection conn = null;
		Map<String, Object> resultMap = new HashMap<String,Object>();
		
		try {
			
		System.out.println(" url :"+url);
					
			//Get Connection
			conn = dbUtil.mGetConnection(url, userName, password);
			
			System.out.println(" conn :"+conn);
			resultMap=new MTXXXDao().fetchAccountsDetailsFromDB(accountDate, accountID, conn);
			
			System.out.println(" system out for result Map acocunt "+resultMap);
			
			if(resultMap.get(IBOSConstants.ACCOUNT_IBAN)==null) {
				
				accountForIntesaandPBZ = new JSONObject("{\r\n" + 
						"  \"Code\": \""+"NOT_FOUND"+"\",\r\n" + 
						"  \"Definition\": \""+"Account Information is not currently accessible"+"\"\r\n" + 
						"}");
				
				return accountForIntesaandPBZ;
			}
			
			
			//Account
			accountForIntesaandPBZ.put(IBOSConstants.account, resultMap.get(IBOSConstants.ACCOUNT_IBAN));
			
			//IBan
			accountForIntesaandPBZ.put(IBOSConstants.iban, resultMap.get(IBOSConstants.ACCOUNT_IBAN));
		
			//Currency
			accountForIntesaandPBZ.put(IBOSConstants.currency, resultMap.get(IBOSConstants.OPEN_BAL_CURRENCY));
			
			/*
			 * if(AHBresultMapString.contains(IBOSConstants.account_short_name)) { //account
			 * short name accountForIntesaandPBZ.put(IBOSConstants.ownerName,
			 * resultMap.get(IBOSConstants.account_short_name)); }
			 */
			if(resultMap.keySet().contains("ACCOUNT_NAME")) {
				//account name
				accountForIntesaandPBZ.put(IBOSConstants.name, resultMap.get("ACCOUNT_NAME"));
			}
			
			//status
			accountForIntesaandPBZ.put(IBOSConstants.status, "enabled"); //defaulted to Enabled
			
			//population of balances
			
			
//			- If available_balance, then BalanceType=forwardAvailable
//			- If booked_balance, then BalanceType=closingBooked
//			- If value_dated_balance, then BalanceType=openingBooked"
			
			if(resultMap.get(IBOSConstants.FWD_AVL_BAL_AMOUNT)!=null) {
				JSONObject tempObj = new JSONObject();
				tempObj.put(IBOSConstants.referenceDate,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				tempObj.put(IBOSConstants.balanceType,"forwardAvailable");
				
				JSONObject balanceAmount = new JSONObject();
				balanceAmount.put(IBOSConstants.amount,resultMap.get(IBOSConstants.FWD_AVL_BAL_AMOUNT));
				balanceAmount.put(IBOSConstants.currency, resultMap.get(IBOSConstants.FWD_AVL_BAL_CURRENCY));
				
				tempObj.put(IBOSConstants.balanceAmount,balanceAmount);
				
				
				
			//	balancesforIntessaAndPBZ.put(tempObj);
			}

			if(resultMap.get(IBOSConstants.OPEN_BAL_AMOUNT)!=null) {
				JSONObject tempObj = new JSONObject();
				tempObj.put(IBOSConstants.referenceDate,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				tempObj.put(IBOSConstants.balanceType,"openingBooked");
				
				JSONObject balanceAmount = new JSONObject();
				balanceAmount.put(IBOSConstants.amount,resultMap.get(IBOSConstants.OPEN_BAL_AMOUNT));
				balanceAmount.put(IBOSConstants.currency, resultMap.get(IBOSConstants.OPEN_BAL_CURRENCY));
				
				tempObj.put(IBOSConstants.balanceAmount,balanceAmount);
				
				/*
				 * if(AHBresultMapString.contains("latest_transaction_booking_date")) {
				 * tempObj.put("lastChangedDateTime",resultMap.get(
				 * "latest_transaction_booking_date")); }
				 */
				
				balancesforIntessaAndPBZ.put(tempObj);
			}
			
			if(resultMap.get(IBOSConstants. CLOSE_BAL_AMOUNT)!=null) {
				JSONObject tempObj = new JSONObject();
				//tempObj.put(IBOSConstants.referenceDate,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				tempObj.put(IBOSConstants.balanceType,"closingBooked");
				
				JSONObject balanceAmount = new JSONObject();
				balanceAmount.put(IBOSConstants.amount,resultMap.get(IBOSConstants.CLOSE_BAL_AMOUNT));
				balanceAmount.put(IBOSConstants.currency, resultMap.get(IBOSConstants.CLOSE_BAL_CURRENCY));
				
				tempObj.put(IBOSConstants.balanceAmount,balanceAmount);
			
				
				
				SimpleDateFormat sff = new SimpleDateFormat("yyMMdd");
				Date tempDate  = sff.parse((String) resultMap.get(IBOSConstants.CLOSE_BAL_DATE));
				tempObj.put(IBOSConstants.lastChangedDateTime, new SimpleDateFormat("yyyy-MM-dd").format(tempDate));
			
				
				/*
				 * if(AHBresultMapString.contains("latest_transaction_booking_date")) {
				 * tempObj.put("lastChangedDateTime",resultMap.get(
				 * "latest_transaction_booking_date")); }
				 */
				
				balancesforIntessaAndPBZ.put(tempObj);
			}
			
			//intessaPBZResponse.put(IBOSConstants.account, accountForIntesaandPBZ);
			accountForIntesaandPBZ.put(IBOSConstants.balances, balancesforIntessaAndPBZ);
			
			System.out.println(" resultMap : "+ accountForIntesaandPBZ);
				
			
		}
		catch (Exception e){
			accountForIntesaandPBZ = new JSONObject("{\r\n" + 
					"  \"Code\": \""+"ERROR_OCCURED"+"\",\r\n" + 
					"  \"Definition\": \""+"Error occurred while accessing account information. The details cannot be returned right now"+"\"\r\n" + 
					"}");
		}
		finally {
			dbUtil.mCloseResources(null, conn);
		}
		
		
		return accountForIntesaandPBZ;
	}

	
	public JSONObject convSantoIntessaJsonForTxn(String accountID, String accountFromDate, String accountToDate , String url, String userName, String password) {
		
		System.out.println(" accountID :"+ accountID +" accountFromDate : "+accountFromDate +" accountToDate "+accountToDate );
		JSONObject intessaPBZResponseForTxn = new JSONObject();
		JSONObject accountForIntesaandPBZ = new JSONObject();
		
		DBConnectionUtil dbUtil = new DBConnectionUtil();
		Connection conn = null;
		List<Map<String, Object>> resultMaplist = new ArrayList();
		
		try {
			
		//Get details
			
			//Get Connection
			conn = dbUtil.mGetConnection(url, userName, password);
			resultMaplist=new MTXXXDao().fetchTransactionsDetailsFromDB(accountToDate,accountFromDate, accountID, conn);
			
			
		System.out.println("Entering Convertor of transactions from Nordea to JSON "+resultMaplist);
		
		if(resultMaplist!=null && resultMaplist.size()==0 ) {
			
			intessaPBZResponseForTxn = new JSONObject("{\r\n" + 
					"  \"Code\": \""+"NOT_FOUND"+"\",\r\n" + 
					"  \"Definition\": \""+"Transaction Information is not currently accessible"+"\"\r\n" + 
					"}");
			
			return intessaPBZResponseForTxn;
		}
		
			//IBan
		accountForIntesaandPBZ.put(IBOSConstants.iban, accountID);
		intessaPBZResponseForTxn.put(IBOSConstants.account, accountForIntesaandPBZ);
		
		/*
		 * //For Transactions JSONObject responseFromNordea = (JSONObject)
		 * resultMaplist.get("response"); JSONArray transactionsFromNordea = (JSONArray)
		 * responseFromNordea.get("transactions");
		 */
		
		JSONArray bookedIntessaTransactions  = new JSONArray();
		JSONArray PendingIntessaTransactions  = new JSONArray();
		JSONObject transactionsFromIntessa = new JSONObject();
		
		
		for(Map<String,Object> tempObject : resultMaplist) {
//			JSONObject nordeaTransaction = (JSONObject) tempObject;
			JSONObject intessaPBZTransaction=new JSONObject();

			intessaPBZTransaction.put("transactionId", tempObject.get("IBOS_TXN_ID"));
			String bookingDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			
			try {
				SimpleDateFormat sff = new SimpleDateFormat("MMdd");
				Date tempDate  = sff.parse((String) tempObject.get(IBOSConstants.ENTRY_DATE_STRING));
				tempDate.setYear(new Date().getYear());
				bookingDate = new SimpleDateFormat("yyyy-MM-dd").format(tempDate);
			}
			catch(Exception e) {
				bookingDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			}
			intessaPBZTransaction.put("bookingDate",bookingDate);
			String valueDAte = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			
			try {
				
				SimpleDateFormat sff = new SimpleDateFormat("yyMMdd");
				Date tempDate  = sff.parse((String) tempObject.get(IBOSConstants.VALUE_DATE_STRING));
				//tempDate.setYear(new Date().getYear());
				valueDAte = new SimpleDateFormat("yyyy-MM-dd").format(tempDate);
				
			}
			catch(Exception e) {
				valueDAte = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			}
		
			intessaPBZTransaction.put("valueDate", valueDAte);
			JSONObject transactionAmount = new JSONObject();
			transactionAmount.put("amount",tempObject.get(IBOSConstants.ACCOuNT_TXN_AMOUNT));
			intessaPBZTransaction.put("transactionAmount", transactionAmount);
			//intessaPBZTransaction.put("debtorName", accountForIntesaandPBZ);
			
			if(tempObject.get(IBOSConstants.NARRATIVE)!=null)intessaPBZTransaction.put("additionalInformation", tempObject.get(IBOSConstants.NARRATIVE));
System.out.println("intessaPBZTransaction.get(\"transactionAmount\").toString()"+intessaPBZTransaction.get("transactionAmount").toString());
			if(intessaPBZTransaction.has("transactionAmount") && intessaPBZTransaction.get("transactionAmount").toString().contains("-")) {
				if(tempObject.get(IBOSConstants.REF_ACC_OWNER)!=null)intessaPBZTransaction.put("debtorName", tempObject.get(IBOSConstants.REF_ACC_OWNER));
				
				PendingIntessaTransactions.put(intessaPBZTransaction);
			}
			else {
				if(tempObject.get(IBOSConstants.REF_ACC_OWNER)!=null)intessaPBZTransaction.put("creditorName", tempObject.get(IBOSConstants.REF_ACC_OWNER));
				
				bookedIntessaTransactions.put(intessaPBZTransaction);
			}
			
			System.out.println(" intessaPBZTransaction   "+intessaPBZTransaction);
			
			
		}
		
		transactionsFromIntessa.put("booked", bookedIntessaTransactions);
		transactionsFromIntessa.put("pending", PendingIntessaTransactions);
		
		intessaPBZResponseForTxn.put("transactions", transactionsFromIntessa);
		
		System.out.println("intessaPBZResponseForTxn :"+ intessaPBZResponseForTxn);
		}
		catch(Exception e) {
			e.printStackTrace();
			intessaPBZResponseForTxn = new JSONObject("{\r\n" + 
					"  \"Code\": \""+"ERROR_OCCURED"+"\",\r\n" + 
					"  \"Definition\": \""+"Error occurred while accessing account information. The details cannot be returned right now "+e.getLocalizedMessage()+"\"\r\n" + 
					"}");
		}
		finally {
			dbUtil.mCloseResources(null, conn);
		}
		
		
		return intessaPBZResponseForTxn;
		
	}
	

}


