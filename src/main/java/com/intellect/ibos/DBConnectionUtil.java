package com.intellect.ibos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import org.springframework.stereotype.Component;

@Component
public class DBConnectionUtil {

	
	public Connection mGetConnection(String url, String userName, String password)
    {
    	Connection conn = null;
//    	 url="jdbc:postgresql://10.240.131.4:5432/IBOSPOC";
//    	 userName = "postgres"; 
//    	 password = "postgres";  	
    	try
    	{
    		Class.forName("org.postgresql.Driver");
    		conn =  DriverManager.getConnection(url,userName,password);
    	}
    	catch(Exception exp)
    	{
    		exp.printStackTrace();
    		System.err.println(exp.getLocalizedMessage());
    	}
    	
    	return conn;
    }
	
	public void mCloseResources(PreparedStatement pstmt, Connection conn) {
		// TODO Auto-generated method stub
		try
		{
			if(pstmt != null)
			{
				pstmt.close();
			}
			if(conn != null)
			{
				conn.close();
			}
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
    		System.err.println(exp.getLocalizedMessage());
		}
	}
    
	
}
