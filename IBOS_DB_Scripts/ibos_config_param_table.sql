INSERT INTO public.ibos_config_param_table (id,param_key,param_value,category,subcategory,description,agreement_id,authorizer_id) VALUES
	 ('12','ACCOUNT_ID','IT03Y0306909473100000003718','BANK','ISP','Intessa Accounts',NULL,NULL),
	 ('13','ACCOUNT_ID','IT42H0306916702100000011076','BANK','ISP','Intessa Accounts',NULL,NULL),
	 ('24','AHB','ISP','HB','SAN','Account holding bank for Santander',NULL,NULL),
	 ('15','ACCOUNT_ID','HR5023400093000000003','BANK','PBZ','PBZ Accounts',NULL,NULL),
	 ('105','ACCOUNT_ID','FI4116603500005114','BANK','NOR','Nordea Accounts','130474822427','70311198'),
	 ('23','ACCOUNT_ID','DE34503205000315883250','BANK','SAN','Santander Accounts',NULL,NULL),
	 ('22','ACCOUNT_ID','DE84503205000315853250','BANK','SAN','Santander Accounts',NULL,NULL),
	 ('21','ACCOUNT_ID','DE96503205000123456250','BANK','SAN','Santander Accounts',NULL,NULL),
	 ('16','ACCOUNT_ID','HR1723400091100000002','BANK','PBZ','PBZ Accounts',NULL,NULL),
	 ('100','ACCOUNT_ID','FI6116603001012213','BANK','NOR','Nordea Accounts','130474822427','70311198');
INSERT INTO public.ibos_config_param_table (id,param_key,param_value,category,subcategory,description,agreement_id,authorizer_id) VALUES
	 ('110','ACCOUNT_ID','FI5115963000005662','BANK','NOR','Nordea Accounts','130348721209','70313276'),
	 ('112','ACCOUNT_ID','FI8015723000311381','BANK','NOR','Nordea Accounts','130474822427','70311198'),
	 ('102','ACCOUNT_ID','FI6116603001005423','BANK','NOR','Nordea Accounts','130474822427','70311198'),
	 ('104','ACCOUNT_ID','FI5815723000312165','BANK','NOR','Nordea Accounts','130474822427','70311198'),
	 ('114','ACCOUNT_ID','FI6711123000311263','BANK','NOR','Nordea Accounts','130348721209','70313276'),
	 ('107','ACCOUNT_ID','FI4715723000311878','BANK','NOR','Nordea Accounts','130474822427','70311198'),
	 ('106','ACCOUNT_ID','FI4616603001014326','BANK','NOR','Nordea Accounts','130348721209','70313276'),
	 ('113','ACCOUNT_ID','FI6816603001007528','BANK','NOR','Nordea Accounts','130474822427','70311198'),
	 ('25','AHB','PBZ','HB','SAN','Account holding bank for Santander
',NULL,NULL),
	 ('26','AHB','NOR','HB','SAN','Account holding bank for Santander
',NULL,NULL);
INSERT INTO public.ibos_config_param_table (id,param_key,param_value,category,subcategory,description,agreement_id,authorizer_id) VALUES
	 ('109','ACCOUNT_ID','FI8216603000004393','BANK','NOR','Nordea Accounts','130348721209','70313276');